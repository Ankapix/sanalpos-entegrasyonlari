<?php
session_start();

require '../../../vendor/autoload.php';

$host_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]";
$path = '/sanalpos-entegrasyonlari/examples/payfor/3d_pay/';
$base_url = $host_url . $path;

$success_url = $base_url . 'response.php';
$fail_url = $base_url . 'response.php';

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
$ip = $request->getClientIp();

$account = [
    'bank'          => 'finansbank',
    'model'         => '3d_pay',
    'mbr_id'     => '5',
    'merchant_id'     => '085300000009704',
    'username'   => 'QNB_API_KULLANICI_3DPAY',
    'password'     => 'UcBN0',
    'store_key'     => '12345678',
    'env'           => 'test',
];

try {
    $pos = new \Ankapix\SanalPos\Pos($account);
} catch (\Ankapix\SanalPos\Exceptions\BankNotFoundException $e) {
    var_dump($e->getCode(), $e->getMessage());
} catch (\Ankapix\SanalPos\Exceptions\BankClassNullException $e) {
    var_dump($e->getCode(), $e->getMessage());
}


$template_title = '3D Model Payment';
