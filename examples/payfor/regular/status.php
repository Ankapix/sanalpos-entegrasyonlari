<?php

require '_config.php';

$template_title = 'Order Status';

require '../../template/_header.php';

// Query Order
$query= $pos->bank->status([
    'order_id'  => '2020102171CC',
]);

$response = $query->response;
$dump = ($response);
?>

<div class="result">
    <dl class="row">
        <dt class="col-sm-12">All Data Dump:</dt>
        <dd class="col-sm-12">
            <pre><?php print_r($dump); ?></pre>
        </dd>
    </dl>
    <hr>
    <div class="text-right">
        <a href="index.php" class="btn btn-lg btn-info">&lt; Click to payment form</a>
    </div>
</div>

<?php require '../../template/_footer.php'; ?>
