<?php

require '_config.php';

$template_title = 'History Order';

require '../../template/_header.php';

// History Order
$query = $pos->bank->history([
    'order_id'  => '2020102171CC',
    'req_date'  => '2020-10-20',
]);

$response = $query->response;
$dump = ($response);
?>

<div class="result">
    <dl class="row">
        <dt class="col-sm-12">All Data Dump:</dt>
        <dd class="col-sm-12">
            <pre><?php print_r($dump); ?></pre>
        </dd>
    </dl>
    <hr>
    <div class="text-right">
        <a href="index.php" class="btn btn-lg btn-info">&lt; Click to payment form</a>
    </div>
</div>

<?php require '../../template/_footer.php'; ?>
