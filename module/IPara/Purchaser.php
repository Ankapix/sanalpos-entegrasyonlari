<?php
namespace IPara;

//Müşteri bilgilerinin bulunduğı sınıfı temsil eder.
class Purchaser {
    public $Name;
    public $Surname;
    public $BirthDate;
    public $Email;
    public $GsmPhone;
    public $IdentityNumber;
    public $ClientIp;  
    public $InvoiceAddress;
    public $ShippingAddress;

    public function __construct()
    {
        $this->InvoiceAddress = new PurchaserAddress();
        $this->ShippingAddress = new PurchaserAddress();
    }
}