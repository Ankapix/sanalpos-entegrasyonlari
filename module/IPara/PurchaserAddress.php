<?php
namespace IPara;


//Müşteri adresi bilgilerinin bulunduğı sınıfı temsil eder.

class PurchaserAddress {
    
    public $Name;
    public $Surname;
    public $Address;
    public $ZipCode;
    public $CityCode;
    public $IdentityNumber;
    public $CountryCode;  
    public $TaxNumber;
    public $TaxOffice;
    public $CompanyName;
    public $PhoneNumber;
}