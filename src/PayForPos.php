<?php

namespace Ankapix\SanalPos;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ankapix\SanalPos\Exceptions\UnsupportedPaymentModelException;
use Ankapix\SanalPos\Exceptions\UnsupportedTransactionTypeException;
use Ankapix\SanalPos\Exceptions\UnknownError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PayForPos
 * @package Ankapix\SanalPos
 */
class PayForPos implements PosInterface
{
    use PosHelpersTrait;

    /**
     * @const string
     */
    public const NAME = 'PayforPay';

    /**
     * API URL
     *
     * @var string
     */
    public $url;

    /**
     * Response Codes
     *
     * @var array
     */
    public $codes = [
        "00" => "Onaylandı",
        "01" => "Bankanizi Arayın",
        "02" => "Bankanızı Arayın (Özel Durum)",
        "03" => "Geçersiz Üye İşyeri",
        "04" => "Karta El Koy",
        "05" => "Onaylanmadi",
        "06" => "Stop List Bildirim Hatasi",
        "07" => "Karta El Koy (Özel Durum)",
        "08" => "Kimlik Sorgula",
        "09" => "Tekrar Deneyin",
        "11" => "Onaylandı (VIP)",
        "12" => "Geçersiz İşlem",
        "13" => "Geçersiz Tutar",
        "14" => "Geçersiz Hesap Numarası",
        "15" => "Tanımsız Issuer",
        "16" => "Asgari odeme duzenli olarak gerceklestirilmedi",
        "22" => "PIN Deneme Sayısı Aştı",
        "25" => "Kayıt Dosyada Bulunamadı",
        "28" => "Orijinal Reddedildi",
        "29" => "Orijinal Bulunmadı",
        "30" => "Mesaj Hatası",
        "32" => "PIN Deneme Sayısı Aştı",
        "33" => "Süresi Dolmuş Kart, El Koy",
        "34" => "Parse Error",
        "35" => "Onaylandı",
        "36" => "Kısıtlı Kart, El Koy",
        "38" => "PIN Deneme Sayısı Aştı",
        "41" => "Kayıp Kart, Kartı Alın",
        "43" => "Çalıntı Kart, Kartı Alın",
        "51" => "Limit Yetersiz",
        "52" => "Tanımlı Hesap Yok",
        "53" => "Tanımlı Hesap Yok",
        "54" => "Süresi Dolmuş Kart",
        "55" => "Yanlış PIN",
        "56" => "Desteklenmeyen Kart",
        "57" => "Karta İzin Verilmeyen İşlem",
        "58" => "POSa İzin Verilmeyen İşlem",
        "61" => "Para Çekme Limiti Aşıldı",
        "62" => "Sınırlı Kart",
        "63" => "Güvenlik İhlali",
        "65" => "Para Çekme Limiti Aşıldı",
        "75" => "PIN Deneme Limiti Aşıldı",
        "76" => "Key Senkronizasyon Hatası",
        "77" => "Red, Script Yok",
        "78" => "Güvenli Olmayan PIN",
        "79" => "ARQC hatası",
        "81" => "Aygıt Versiyon Uyuşmazlığı",
        "85" => "Onaylandı",
        "91" => "Issuer çalışmıyor",
        "92" => "Finansal Kurum Tanınmıyor",
        "93" => "Internete Kapalı Kart",
        "95" => "POS Günsonu Hatası",
        "96" => "Sistem Hatası",
        "98" => "Çift Ters İşlem",
        "99" => "Genel Hata",
        "CS000" => "Tanımsız operasyon",
        "CS001" => "Hatalı istek mesajı",
        "CS002" => "Yetkiniz bulunmamaktadır",
        "CS003" => "Hatalı tamamlandı",
        "F001" => "İşlem Fraud tarafından red edildi.",
        "M001" => "Bonus miktarı sipariş miktarından büyük olamaz.",
        "M002" => "Para birimi kodu geçersiz.",
        "M003" => "Para birimi kodu eksik.",
        "M004" => "Sıfır veya boş veya hatalı miktar.",
        "M005" => "Cvv2 boş olamaz.",
        "M006" => "Son kullanma tarihi eksik yada hatalı, 4 karakter olmalıdır.",
        "M007" => "Fail Url eksik.",
        "M008" => "Pan eksik yada geçersiz uzunlukta. Pan 13 ile 19 karakter arasında olmalıdır.",
        "M009" => "Şifre eşleşmesi başarılı değil, lütfen şifrenizi onaylayınız.",
        "M010" => "Ok Url hatalı.",
        "M011" => "Pareq hazırlanamadı.",
        "M012" => "Satın alma miktarı eksik yada geçersiz uzunlukta.",
        "M013" => "Kurum kodu eksik.",
        "M014" => "Bilinmeyen güvenlik tipi.",
        "M015" => "Bilinmeyen i?lem tipi.",
        "M016" => "Kullanıcı kodu eksik.",
        "M017" => "Kullanıcı şifresi eksik.",
        "M018" => "Hatalı Cvv2 {0}",
        "M019" => "İşlem tutarı 0,01 - 200.000 arasında olmalıdır.",
        "M020" => "Full puan kullanımlı işlemde taksit yapılamaz.",
        "M021" => "Bu işlem tipi için geçersiz güvenlik tipi",
        "M022" => "Bu işlem tipi için orijinal işlem sipariş numarası boş olamaz",
        "M023" => "Bu işlem tipi için orijinal işlem sipariş numarası gönderilmemelidir.",
        "M024" => "Üye işyeri numarası eksik.",
        "M025" => "{0} alanı hatalı uzunlukta Gelen : {1} Olması Gereken : {2}",
        "M026" => "Bu işlem tipi için hatalı güvenlik tipi",
        "M027" => "Aradığınız kayıt bulunamadı",
        "M028" => "Bu işlem tipi için sipariş numarası boş olamaz",
        "M030" => "HASH Uyusmazligi",
        "M038" => "OkURL en fazla 200 karakter olabilir",
        "M041" => "Geçersiz kart numarası",
        "M042" => "Plugin bulunamadı",
        "M043" => "Request formu boş olamaz",
        "M044" => "Mpi post sırasında hata oluştu",
        "M045" => "Max Miktar Hatası",
        "M046" => "Rapor isteği sırasında hata oluştu",
        "M047" => "Bu kullanıcı için ip kısıtlaması vardır.",
        "M048" => "Hatalı Değer ",
        "M049" => "Sisteme kimlik doğrulaması yapılamadı. Kullanıcı:{0} Şifre:{1}",
        "M050" => "Bu para birimine yetkiniz yoktur",
        "M051" => "MD de üye işyeri bilgisi eksik.",
        "M052" => "Puan tutarı sipariş tutarından farklı olamaz",
        "M053" => "İzin verilmeyen karakter",
        "M61" => "Oturum süresi aşıldı, yeniden işlem gönderin.",
        "MR00" => "Hatalı Request",
        "MR04" => "ExpiryDate alanı boş",
        "MR05" => "ExpiryDate alanı hatalı",
        "MR06" => "Pan alanı hatalı",
        "MR07" => "CardBrand alanı hatalı",
        "MR08" => "PurchAmount hatalı",
        "MR09" => "Currency alanı boş",
        "MR11" => "MerchantCode alanı boş",
        "MR12" => "MerchantPass alanı boş",
        "MR13" => "AlphaCode alanı boş",
        "MR15" => "3D Secure Doğrulama hatası",
        "MR16" => "3D Desteklenmeyen Kart Markası",
        "MR20" => "Pares mesaji parse edilemedi",
        "MR21" => "Pares response hatası",
        "MR22" => "Pares imza doğrulama başarısız",
        "MR3D" => "Parse Error",
        "P001" => "Bu kayıt zaten tanımlanmıştır.",
        "P002" => "İlgili kayıt bulunamadı.",
        "V000" => "İşlem tamamlanamadı /devam ediyor",
        "V001" => "Üye işyeri bulma hatası.",
        "V002" => "Hata çözümüne yada DS e bağlanılamadı. {0}",
        "V003" => "Sistem kapali.",
        "V004" => "Hatalı kullanıcı adı veya şifre",
        "V005" => "Bilinmeyen kart tipi.",
        "V006" => "Kullanıcıya bu işlem tipi için izin verilmemiş.",
        "V007" => "Terminal aktif değil.",
        "V008" => "Üye işyeri bulunamadı.",
        "V009" => "Üye işyeri aktif değil.",
        "V010" => "Terminal bu işlem tipi için yetkili değil.",
        "V011" => "İşlem tipine bu terminal için izin verilmemiş.",
        "V012" => "Pareq Hatası {0}",
        "V013" => "Seçili İşlem Bulunamadı!",
        "V014" => "Bu işlem geri alınamaz, lüften asıl işlemi iptal edin.",
        "V015" => "Girilen iade miktarı asıl işlem tutarşndan büyük olamaz!",
        "V016" => "Seçili işlem önprovizyon işlemi değildir!",
        "V017" => "Sipariş daha önce iptal edilmiştir.",
        "V018" => "Bu işlem iptal edilemez.",
        "V019" => "Kısmı iptale izin verilemez, işlem değeri değiştirilemez.",
        "V020" => "Bu işlem henüz tamamlanmamıştır",
        "V021" => "Asıl  işlem boş olmaz.",
        "V022" => "Orjinal provizyon no belirtilen işlem tipi için seçilemez.",
        "V023" => "Taksit sayısı belirtilmelidir.",
        "V024" => "Taksit sayısı belirtilen işlem tipi için sıfırdan büyük olamaz.",
        "V025" => "Bu provizyon önceden kapatilmistir",
        "V026" => "Provizyon süresi dolmus",
        "V027" => "Girilen tutar orjinal tutarın %15 kadar fazla veya eksik girilebilir.",
        "V028" => "Bilinmeyen işlem türü",
        "V029" => "Order id tekil olmalıdır",
        "V030" => "Para birimi bulunamadı.",
        "V031" => "İade edilecek tutar girilmemiş",
        "V032" => "Batch No bulunamadı",
        "V033" => "3D Kullanıcı Doğrulama Adımı Başarılı",
        "V034" => "3D Kullanıcı Doğrulama Adımı Başarısız",
        "V035" => "Kullanıcı bu işlem için izin verilmemiş.",
        "V036" => "Sistem hatası",
        "V037" => "Sipariş daha önce iade edilmiştir.",
        "V038" => "Bu işlem tipi için yetkili değilsiniz.",
        "V040" => "Tekrar Sayısı alanı hatalı",
        "V041" => "Aralık Tipi Hatalı",
        "V042" => "Hatalı Aralık Süresi",
        "V043" => "Hatalı Müşteri Kodu",
        "V044" => "Ödeme başarısız",
        "V045" => "Eksik Alt Üye İşyeri Parametreleri",
        "V046" => "Ticari Kart parametreleri ayrıştırılamadı",
        "V047" => "Ticari Kart kullanılan puan 0 olamaz",
        "V048" => "Ticari Kart iki taksit arası fark tanımlanandan küçük olamaz",
        "V049" => "Ticari Kart iki taksit arası fark tanımlanandan büyük olamaz",
        "V050" => "Ticari Kart toplam taksit tutarları satış tutarına eşit olmalıdır",
        "V051" => "Ticari Kart son vade günü tanımlanan azami vade gün sayısından büyük olamaz",
        "V052" => "Ticari Kart ilk vade günü tanımlanan ilk vade gün sayısından az olamaz",
        "V053" => "Ticari Kart taksit tutarı ve vade tarihi boş olamaz",
        "V054" => "Ticari Kart taksit tutarı 0 olamaz",
        "V055" => "Ticari Kart vade tarihi ya da taksit tutarı boş",
        "V056" => "Ticari Kart taksit sayısı tanımlanan taksit sayısından fazla olamaz",
        "V057" => "Ticari Kart taksit sayısı 1 den büyük olmalıdır",
        "V058" => "Ticari Kart vade gün sayısı tanımlanandan fazla olamaz",
        "V059" => "Ticari Kart vade gün sayısı boş",
        "V060" => "Ticari Kart son vade tarihi tanımlı asgari günden önce olamaz",
        "V061" => "Hatalı TCKN",
        "V061" => "Hatalı TCKN veya VKN uzunluğu",
        "V061" => "Hatalı VKN",
        "V061" => "Müşteri Kodu boş",
        "V061" => "Sigorta ödemesi için hatalı pan ",
        "V111" => "Test işlemi için tanımlı olmayan kart",
        "V9196" => "Bağlantı Hatası",
        "V9199" => "Banka bağlantısında hata oluştu."
    ];

    /**
     * Transaction Types
     *
     * @var array
     */
    public $types = [
        'pay'   => 'Auth'
    ];

    /**
     * Currencies
     *
     * @var array
     */
    public $currencies = [];

    /**
     * Transaction Type
     *
     * @var string
     */
    public $type;

    /**
     * API Account
     *
     * @var array
     */
    protected $account = [];

    /**
     * Order Details
     *
     * @var array
     */
    protected $order = [];

    /**
     * Credit Card
     *
     * @var object
     */
    protected $card;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response Raw Data
     *
     * @var object
     */
    protected $data;

    /**
     * Processed Response Data
     *
     * @var mixed
     */
    public $response;

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Mode
     *
     * @var string
     */
    protected $mode = 'PROD';

    /**
     * API version
     * @var string
     */
    protected $version = '1.0.2';

    /**
     * GarantiPost constructor.
     *
     * @param array $config
     * @param array $account
     * @param array $currencies
     */
    public function __construct($config, $account, array $currencies)
    {
        $request = Request::createFromGlobals();
        $this->request = $request->request;

        $this->config = $config;
        $this->account = $account;
        $this->currencies = $currencies;

        $this->url = isset($this->config['urls'][$this->account->env]) ?
            $this->config['urls'][$this->account->env] :
            $this->config['urls']['production'];

        if ($this->account->env == 'test') {
            $this->mode = 'TEST';
        }

        return $this;
    }

    /**
     * Create 3D Hash
     *
     * @return string
     */
    public function create3DHash()
    {
        $map = [
            $this->account->mbr_id,
            $this->order->id,
            $this->amountFormat($this->order->amount),
            $this->order->success_url,
            $this->order->fail_url,
            $this->type,
            $this->order->installment>1?$this->order->installment:"0",
            $this->order->rand,
            $this->account->store_key
        ];

        return base64_encode(pack('H*',sha1(implode('', $map))));
    }

    /**
     * Amount Formatter
     *
     * @param double $amount
     * @return int
     */
    public function amountFormat($amount)
    {
        return (double) $amount;
    }

    /**
     * Create Regular Payment 
     *
     * @return string
     */
    protected function createRegularPayment()
    {
        try {
            $InputsList = [
                'MbrId'            =>  $this->account->mbr_id,
                'MerchantID'       =>  $this->account->merchant_id,
                'UserCode'         =>  $this->account->username,
                'UserPass'         =>  $this->account->password,
                'OrderId'          =>  $this->order->id,
                'OrgOrderId'       =>  "",
                'SecureType'       =>  "NonSecure",
                'TxnType'          =>  $this->type,
                'PurchAmount'      =>  $this->amountFormat($this->order->amount),
                'Currency'         =>  $this->order->currency,
                'Pan'              =>  $this->card->number,
                'Expiry'           =>  $this->card->month . $this->card->year,
                'Cvv2'             =>  $this->card->cvv,
                'MOTO'             =>  "0",
                'Lang'             =>  isset($this->order->lang) ?strtoupper($this->order->lang):"TR",
                'InstallmentCount' => $this->order->installment > 1 ? $this->order->installment : ''
            ];
            $InputText = "";
            foreach($InputsList as $Key =>$Data){
                $InputText .= $Key."=".$Data."&";
            }
            $this->send(rtrim($InputText, "&"));
            $resultValues   = explode(";;", $this->data);
            $data           = [];
            foreach($resultValues as $resultt)
            {
                if($resultt){
                    list($key,$value)= explode("=", $resultt);
                    $data[$key] = $value;
                }
            }
            $this->data = (Object) $data;
        } catch (\Exception $e) {
            throw new UnknownError($e->getMessage(), 400);
        }
        return $this;
    }

    /**
     * Create 3D Payment 
     * @return string
     */
    protected function create3DPayment()
    {
        $InputsList = [
            'RequestGuid'      =>  $this->request->get('RequestGuid'),
            'OrderId'          =>  $this->request->get('OrderId'),
            'UserCode'         =>  $this->account->username,
            'UserPass'         =>  $this->account->password,
            'SecureType'       =>  '3DModelPayment'
        ];
        $InputText = "";
        foreach($InputsList as $Key =>$Data){
            $InputText .= $Key."=".$Data."&";
        }
        $this->send(rtrim($InputText, "&"));
        $resultValues   = explode(";;", $this->data);
        $data           = [];
        foreach($resultValues as $resultt)
        {
            if($resultt){
                list($key,$value)= explode("=", $resultt);
                $data[$key] = $value;
            }
        }
        $this->data = (Object) $data;
        return $this;
    }

    /**
     * Get ProcReturnCode
     *
     * @return string|null
     */
    protected function getProcReturnCode()
    {
        return isset($this->data->ProcReturnCode) ? (string) $this->data->ProcReturnCode : null;
    }

    /**
     * Get Status Detail Text
     *
     * @return string|null
     */
    protected function getStatusDetail()
    {
        $proc_return_code =  $this->getProcReturnCode();

        return $proc_return_code ? (isset($this->codes[$proc_return_code]) ? (string) $this->codes[$proc_return_code] : null) : null;
    }

    /**
     * Regular Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function makeRegularPayment()
    {
        $contents = $this->createRegularPayment();

        $response = 'Başarısız İşlem';
        if ($this->getProcReturnCode() == '00' and $this->data->TxnResult=="Success") {
            $response = 'Başarılı İşlem';
            $status = 'approved';
        }

        $this->response = (object) [
            'id'                => $this->order->id,
            'order_id'          => isset($this->data->OrderId) ? $this->data->OrderId : null,
            'org_order_id'      => isset($this->data->OrgOrderId) ? $this->data->OrgOrderId : null,
            'trans_id'          => isset($this->data->RequestGuid) ? $this->data->RequestGuid : null,
            'response'          => $response,
            'transaction_type'  => $this->type,
            'transaction'       => $this->order->transaction,
            'auth_code'         => isset($this->data->AuthCode) ? $this->data->AuthCode : null,
            'host_ref_num'      => isset($this->data->HostRefNum) ? $this->data->HostRefNum : null,
            'rrn'               => isset($this->data->RRN) ? $this->data->RRN : null,
            'hash_data'         => isset($this->data->Hash) ? $this->data->Hash : null,
            'proc_return_code'  => $this->getProcReturnCode(),
            'code'              => $this->getProcReturnCode(),
            'status'            => $status,
            'status_detail'     => $this->getStatusDetail(),
            'currency'          => isset($this->data->Currency) ? $this->data->Currency : null,
            'installment'       => isset($this->data->InstallmentCount) ? $this->data->InstallmentCount : null,
            'error_code'        => isset($this->data->ProcReturnCode) ? $this->data->ProcReturnCode : null,
            'error_message'     => isset($this->data->ErrMsg) ? $this->data->ErrMsg : null,
            'batch_no'          => isset($this->data->BatchNo) ? $this->data->BatchNo : null,
            'all'               => $this->data,
        ];

        return $this;
    }

    /**
     * Make 3D Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function make3DPayment()
    {
        $status = 'declined';
        $response = 'Başarısız İşlem';
        $proc_return_code = '99';
        if ($this->request->get('3DStatus') =="1") {
            $this->create3DPayment();
            if ($this->getProcReturnCode() == '00' and $this->data->TxnResult=="Success") {
                $response = 'Başarılı İşlem';
                $proc_return_code = $this->data->ProcReturnCode;
                $status = 'approved';
            }
        }

        $this->response = (object) [
            'id'                => $this->order->id,
            'order_id'          => isset($this->data->OrderId) ? $this->data->OrderId : null,
            'org_order_id'      => isset($this->data->OrgOrderId) ? $this->data->OrgOrderId : null,
            'trans_id'          => isset($this->data->RequestGuid) ? $this->data->RequestGuid : null,
            'response'          => $response,
            'transaction_type'  => $this->type,
            'transaction'       => $this->order->transaction,
            'transaction_security' => isset($this->data->SecureType) ? $this->data->SecureType : null,
            'auth_code'         => isset($this->data->AuthCode) ? $this->data->AuthCode : null,
            'host_ref_num'      => isset($this->data->HostRefNum) ? $this->data->HostRefNum : null,
            'rrn'               => isset($this->data->RRN) ? $this->data->RRN : null,
            'hash_data'         => isset($this->data->Hash) ? $this->data->Hash : null,
            'eci'               => isset($this->data->Eci) ? $this->data->Eci : null,
            'response_hash'     => isset($this->data->ResponseHash) ? $this->data->ResponseHash : null,
            'proc_return_code'  => $proc_return_code,
            'code'              => $this->getProcReturnCode(),
            'status'            => $status,
            'status_detail'     => $this->getStatusDetail(),
            'currency'          => isset($this->data->Currency) ? $this->data->Currency : null,
            'installment'       => isset($this->data->InstallmentCount) ? $this->data->InstallmentCount : null,
            'error_code'        => isset($this->data->ProcReturnCode) ? $this->data->ProcReturnCode : null,
            'error_message'     => isset($this->data->ErrMsg) ? $this->data->ErrMsg : null,
            'batch_no'          => isset($this->data->BatchNo) ? $this->data->BatchNo : null,
            'all'               => $this->data,
            '3d_all'            => $this->request->all(),
        ];

        return $this;
    }

    /**
     * Make 3D Pay Payment
     *
     * @return $this
     */
    public function make3DPayPayment()
    {

        $status = 'declined';
        $response = 'Başarısız İşlem';
        $proc_return_code = '99';
        if ($this->request->get('3DStatus') =="1") {
            $this->data = (Object) $this->request->all();
            if ($this->getProcReturnCode() == '00' and $this->data->TxnResult=="Success") {
                $response = 'Başarılı İşlem';
                $proc_return_code = $this->data->ProcReturnCode;
                $status = 'approved';
            }
        }

        $this->response = (object) [
            'id'                => $this->order->id,
            'order_id'          => isset($this->data->OrderId) ? $this->data->OrderId : null,
            'org_order_id'      => isset($this->data->OrgOrderId) ? $this->data->OrgOrderId : null,
            'trans_id'          => isset($this->data->RequestGuid) ? $this->data->RequestGuid : null,
            'response'          => $response,
            'transaction_type'  => $this->type,
            'transaction'       => $this->order->transaction,
            'transaction_security' => isset($this->data->SecureType) ? $this->data->SecureType : null,
            'auth_code'         => isset($this->data->AuthCode) ? $this->data->AuthCode : null,
            'host_ref_num'      => isset($this->data->HostRefNum) ? $this->data->HostRefNum : null,
            'rrn'               => isset($this->data->RRN) ? $this->data->RRN : null,
            'hash_data'         => isset($this->data->Hash) ? $this->data->Hash : null,
            'eci'               => isset($this->data->Eci) ? $this->data->Eci : null,
            'response_hash'     => isset($this->data->ResponseHash) ? $this->data->ResponseHash : null,
            'proc_return_code'  => $proc_return_code,
            'code'              => $this->getProcReturnCode(),
            'status'            => $status,
            'status_detail'     => $this->getStatusDetail(),
            'currency'          => isset($this->data->Currency) ? $this->data->Currency : null,
            'installment'       => isset($this->data->InstallmentCount) ? $this->data->InstallmentCount : null,
            'error_code'        => isset($this->data->ProcReturnCode) ? $this->data->ProcReturnCode : null,
            'error_message'     => isset($this->data->ErrMsg) ? $this->data->ErrMsg : null,
            'batch_no'          => isset($this->data->BatchNo) ? $this->data->BatchNo : null,
            'all'               => $this->data
        ];

        return $this;
    }

    /**
     * Get 3d Form Data
     *
     * @return array
     */
    public function get3DFormData()
    {
        $hash_data = $this->create3DHash();
        $inputs = [
            'Hash'             =>  $hash_data,
            'MbrId'            =>  $this->account->mbr_id,
            'MerchantID'       =>  $this->account->merchant_id,
            'UserCode'         =>  $this->account->username,
            'UserPass'         =>  $this->account->password,
            'OrderId'          =>  $this->order->id,
            'OrgOrderId'       =>  "",
            'SecureType'       =>  $this->account->model == '3d_pay' ? '3DPay' : '3DModel',
            'TxnType'          =>  $this->type,
            'PurchAmount'      =>  $this->amountFormat($this->order->amount),
            'Currency'         =>  $this->order->currency,
            'Pan'              =>  $this->card->number,
            'Expiry'           =>  $this->card->month . $this->card->year,
            'Cvv2'             =>  $this->card->cvv,
            'Lang'             =>  isset($this->order->lang) ?strtoupper($this->order->lang):"TR",
            'InstallmentCount' => $this->order->installment > 1 ? $this->order->installment : '0',
            'Rnd'              => $this->order->rand,
            'OkUrl'            => $this->order->success_url,
            'FailUrl'          => $this->order->fail_url
        ];

        return [
            'gateway'       => $this->url,
            'success_url'   => $this->order->success_url,
            'fail_url'      => $this->order->fail_url,
            'rand'          => $this->order->rand,
            'hash'          => $hash_data,
            'inputs'        => $inputs,
        ];
    }
    /**
    * Get 3d Form 
    *
    * @return array
    */
    public function get3DForm()
    {
        $form_data = (array) $this->get3DFormData();
        $return = '<form method="post" action="'.$form_data['gateway'].'"  name="3dForm" class="redirect-form" role="form">';
        foreach ($form_data['inputs'] as $key => $value){
            $return .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
        }
        $return .= '<div class="text-center">Yönlendiriliyorsunuz...</div> <hr>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-lg btn-block btn-success">Ödeme Doğrulaması Yap</button>
            </div>
        </form>
        <script type="text/javascript">document.forms[0].submit();</script>';
        return $return;
    }

    /**
     * Send contents to WebService
     *
     * @param $contents
     * @return $this
     * @throws GuzzleException
     */
    public function send($contents)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 90);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $contents);
            $this->data = curl_exec($ch);
            if (curl_errno($ch)) {                         
                throw new UnknownError(curl_error($ch), 400);              
            } else {                                       
               curl_close($ch);                            
            }   
        } catch (\Exception $e) {
            throw new UnknownError($e->getMessage(), 400);
        }
        return $this;
    }

    /**
     * Prepare Order
     *
     * @param object $order
     * @param object null $card
     * @return mixed
     * @throws UnsupportedTransactionTypeException
     */
    public function prepare($order, $card = null)
    {
        $this->type = $this->types['pay'];
        if (isset($transaction)) {
            if (array_key_exists($transaction, $this->types)) {
                $this->type = $this->types[$transaction];
            } else {
                throw new UnsupportedTransactionTypeException('Unsupported transaction type!');
            }
        }

        $this->order = $order;
        $this->card = $card;

        if ($this->card) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            if(strlen($this->card->year)==4)
            $this->card->year  = substr($this->card->year,-2,2);
        }
        if ($this->order) {
            $this->order->installment = $this->order->installment>1?$this->order->installment:"0";
        }
    }

    /**
     * Make Payment
     *
     * @param object $card
     * @return mixed
     * @throws UnsupportedPaymentModelException
     * @throws GuzzleException
     */
    public function payment($card)
    {
        $this->card = $card;
        if (count((array) $card)) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            if(strlen($this->card->year)==4)
            $this->card->year  = substr($this->card->year,-2,2);
        }
        $model = 'regular';
        if (isset($this->account->model) && $this->account->model) {
            $model = $this->account->model;
        }

        if ($model == 'regular') {
            $this->makeRegularPayment();
        } elseif ($model == '3d') {
            $this->make3DPayment(); 
        } elseif ($model == '3d_pay') {
            $this->make3DPayPayment();
        } else {
            throw new UnsupportedPaymentModelException();
        }

        return $this;
    }

    /**
     * Refund or Cancel Order
     *
     * @param array $meta
     * @param $type
     * @return $this
     * @throws GuzzleException
     */
    protected function refundOrCancel(array $meta, $type)
    {
        $this->order = (object) [
            'id'        => $meta['order_id'],
            'amount'    => isset($meta['amount']) ? $meta['amount'] : null,
            'lang'     => isset($meta['lang']) ? strtoupper($meta['lang']) : "TR",
        ];
        $currency = (int) $this->currencies[$meta['currency']];

        $InputsList = [
            'MbrId'            =>  $this->account->mbr_id,
            'MerchantID'       =>  $this->account->merchant_id,
            'UserCode'         =>  $this->account->username,
            'UserPass'         =>  $this->account->password,
            'OrgOrderId'       =>  $this->order->id,
            'Lang'             =>  $this->order->lang,
            'TxnType'          =>  $type,
            'Currency'         =>  $currency,
            'SecureType'       =>  "NonSecure"
        ];
        if($type=="Refund"){
            $InputsList['PurchAmount']  = $this->amountFormat($this->order->amount);
        }
        $InputText = "";
        foreach($InputsList as $Key =>$Data){
            $InputText .= $Key."=".$Data."&";
        }
        $this->send(rtrim($InputText, "&"));
        $resultValues   = explode(";;", $this->data);
        $data           = [];
        foreach($resultValues as $resultt)
        {
            if($resultt){
                list($key,$value)= explode("=", $resultt);
                $data[$key] = $value;
            }
        }
        $this->data = (Object) $data;

        $status = 'declined';
        if ($this->getProcReturnCode() == '00') {
            $status = 'approved';
        }

        $this->response = (object) [
            'id'                => $this->order->id,
            'order_id'          => isset($this->data->OrderId) ? $this->data->OrderId : null,
            'org_order_id'      => isset($this->data->OrgOrderId) ? $this->data->OrgOrderId : null,
            'trans_id'          => isset($this->data->RequestGuid) ? $this->data->RequestGuid : null,
            'response'          => $this->getStatusDetail(),
            'host_ref_num'      => isset($this->data->HostRefNum) ? $this->data->HostRefNum : null,
            'rrn'               => isset($this->data->RRN) ? $this->data->RRN : null,
            'hash_data'         => isset($this->data->Hash) ? $this->data->Hash : null,
            'proc_return_code'  => $this->getProcReturnCode(),
            'code'              => $this->getProcReturnCode(),
            'status'            => $status,
            'status_detail'     => $this->getStatusDetail(),
            'currency'          => isset($this->data->Currency) ? $this->data->Currency : null,
            'installment'       => isset($this->data->InstallmentCount) ? $this->data->InstallmentCount : null,
            'error_code'        => isset($this->data->ProcReturnCode) ? $this->data->ProcReturnCode : null,
            'error_message'     => isset($this->data->ErrMsg) ? $this->data->ErrMsg : null,
            'batch_no'          => isset($this->data->BatchNo) ? $this->data->BatchNo : null,
            'all'               => $this->data,
        ];

        return $this;
    }

    /**
     * Refund Order
     *
     * @param $meta
     * @return $this
     * @throws GuzzleException
     */
    public function refund(array $meta)
    {
        return $this->refundOrCancel($meta, 'Refund');
    }

    /**
     * Cancel Order
     *
     * @param array $meta
     * @return $this
     * @throws GuzzleException
     */
    public function cancel(array $meta)
    {
        return $this->refundOrCancel($meta, 'Void');
    }

    /**
     * Order Status or History
     *
     * @param array $meta
     * @param $type
     * @return $this
     * @throws GuzzleException
     */
    protected function statusOrHistory(array $meta, $type)
    {
        $this->order = (object) [
            'id'        => $meta['order_id'],
            'req_date'  => isset($meta['req_date']) ? date("Ymd", strtotime($meta['req_date'])) : null,
            'lang'     => isset($meta['lang']) ? strtoupper($meta['lang']) : "TR",
        ];

        $InputsList = [
            'MbrId'            =>  $this->account->mbr_id,
            'MerchantID'       =>  $this->account->merchant_id,
            'UserCode'         =>  $this->account->username,
            'UserPass'         =>  $this->account->password,
            'Lang'             =>  $this->order->lang,
            'TxnType'          =>  $type,
            'SecureType'       =>  $type=="Inquiry"?"OrderInquiry":"Report"
        ];
        if($type=="Report"){
            $InputsList['ReqDate']  = $this->order->req_date;
            if($this->order->id){
                $InputsList['OrderId']  = $this->order->id;
            }
        }else{
            $InputsList['OrgOrderId']  = $this->order->id;
        }
        $InputText = "";
        foreach($InputsList as $Key =>$Data){
            $InputText .= $Key."=".$Data."&";
        }
        $this->send(rtrim($InputText, "&"));

        $this->response = (Object) json_decode($this->data);

        return $this;
    }

    /**
     * Order Status
     *
     * @param array $meta
     * @return $this
     * @throws GuzzleException
     */
    public function status(array $meta)
    {
        return $this->statusOrHistory($meta, 'Inquiry');
    }

    /**
     * Order History
     *
     * @param array $meta
     * @return $this
     * @throws GuzzleException
     */
    public function history(array $meta)
    {
        return $this->statusOrHistory($meta, 'TxnHistory');
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
    * Installment List
    *
    * @return $this
    */
    public function getInstallmentList(array $meta)
    {
    }

    /**
    * setCard
    *
    * @return $this
    */
    public function setCard($card=null)
    {
    }
}
