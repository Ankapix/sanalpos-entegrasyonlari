<?php

namespace Ankapix\SanalPos;

use GuzzleHttp\Exception\GuzzleException;
use Ankapix\SanalPos\Exceptions\UnsupportedPaymentModelException;
use Ankapix\SanalPos\Exceptions\UnsupportedTransactionTypeException;
use Ankapix\SanalPos\Exceptions\UnknownError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PayTrPos
 * @package Ankapix\SanalPos
 */
class PayTrPos implements PosInterface
{
    use PosHelpersTrait;

    /**
     * @const string
     */
    public const NAME = 'PayTrPos';

    /**
     * API URL
     *
     * @var string
     */
    public $url;
    /**
     * Response Codes
     *
     * @var array
     */
    public $codes = [
        '0'    => "Başarısız işlem",
        '1'    => "Kimlik Doğrulama yapılmadı. Lütfen tekrar deneyin ve işlemi tamamlayın.",
        '2'    => "Kimlik Doğrulama başarısız. Lütfen tekrar deneyin ve şifreyi doğru girin.",
        '3'    => "Güvenlik kontrolü sonrası onay verilmedi veya kontrol yapılamadı.",
        '6'    => "İzin verilen sürede ödeme tamamlanmadı.",
        '8'    => "Bu karta taksit yapılamamaktadır.",
        '9'    => "Bu kart ile işlem yetkisi bulunmamaktadır.",
        '10'    => "Bu işlemde 3D Secure kullanılmalıdır.",
        '11'    => "Güvenlik uyarısı. İşlem yapan müşterinizi kontrol edin.",
        '99'    => "İşlem başarısız: Teknik entegrasyon hatası."
    ];

    /**
     * Transaction Types
     *
     * @var array
     */
    public $types = [
        'pay'   => '1'
    ];

    /**
     * Currencies
     *
     * @var array
     */
    public $currencies = [];

    /**
     * Transaction Type
     *
     * @var string
     */
    public $type;

    /**
     * API Account
     *
     * @var array
     */
    protected $account = [];

    /**
     * Order Details
     *
     * @var array
     */
    protected $order = [];

    /**
     * Credit Card
     *
     * @var object
     */
    protected $card;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response Raw Data
     *
     * @var object
     */
    protected $data;

    /**
     * Processed Response Data
     *
     * @var mixed
     */
    public $response;

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Mode
     *
     * @var string
     */
    protected $mode = 'PROD';

    /**
     * API version
     * @var string
     */
    protected $version = 'V1.0';

    /**
     * GarantiPost constructor.
     *
     * @param array $config
     * @param array $account
     * @param array $currencies
     */
    public function __construct($config, $account, array $currencies)
    {
        $request = Request::createFromGlobals();
        $this->request = $request->request;

        $this->config = $config;
        $this->account = $account;
        $this->currencies = $currencies;

        $this->url = isset($this->config['urls'][$this->account->env]) ?
            $this->config['urls'][$this->account->env] :
            $this->config['urls']['production'];

        if ($this->account->env == 'test') {
            $this->mode = 'TEST';
        }

        return $this;
    }

    /**
     * Amount Formatter
     *
     * @param double $amount
     * @return float
     */
    public function amountFormat($amount)
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * Get ProcReturnCode
     *
     * @return string|null
     */
    protected function getProcReturnCode(): ?string
    {
        return isset($this->data->failed_reason_code) ? (string) $this->data->failed_reason_code : null;
    }

    /**
     * Get Status Detail Text
     *
     * @return string|null
     */
    protected function getStatusDetail(): ?string
    {
        $proc_return_code = $this->getProcReturnCode();

        return $proc_return_code ? (isset($this->codes[$proc_return_code]) ? (string) $this->codes[$proc_return_code] : null) : null;
    }
    /**
     * Create 3D Hash
     *
     * @return string
     */
    public function create3DHash(): string
    {
        $map = [
            $this->account->merchant_id,
            isset($this->order->ip) ?$this->order->ip:$this->getIpAdress(),
            $this->order->id,
            trim($this->order->email)?:'msn@msn.com',
            $this->amountFormat($this->order->amount),
            "card",
            $this->order->installment>1?$this->order->installment:"0",
            $this->order->currency,
            $this->mode=='TEST'?1:"0",
            $this->account->model != '3d' ? '1' : '0',
            $this->account->merchant_salt
        ];

        return base64_encode(hash_hmac('sha256',implode('', $map),$this->account->merchant_key,true));
    }
    /**
     * Make 3D Payment
     *
     * @return $this
     */
    public function make3DPayment(): PayTrPos

    {
        $this->request  = Request::createFromGlobals();
        $this->data     = $this->request->request->all();
        $status         = 'declined';
        $response       = 'Başarısız İşlem';
        $hash       	= base64_encode( hash_hmac('sha256', $this->request->get('merchant_oid').$this->account->merchant_salt.$this->request->get('status').$this->request->get('total_amount'), $this->account->merchant_key, true) );
        
        if( $hash != $this->request->get('hash')){
            $status = 'declined';
            $response = 'Başarısız İşlem';
        }elseif ($this->request->get('status') == 'success') {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object)[
            'id' => $this->request->get('merchant_oid'),
            'response' => $status=='approved'?$response:$this->request->get('failed_reason_msg'),
            'transaction_type' => $this->type,
            'transaction' => $this->order->transaction,
            'status' => $status,
            'status_detail' => $this->getStatusDetail(),
            'error_code' => $this->request->get('failed_reason_code')?: null,
            'error_message' => $this->request->get('failed_reason_msg')?: null,
            'hash' => (string)$this->request->get('hash'),
            'total_amount' => (string)$this->request->get('total_amount'),
            'test_mode' => (string)$this->request->get('test_mode'),
            'payment_type' => (string)$this->request->get('payment_type'),
            'currency' => (string)$this->request->get('currency'),
            'payment_amount' => (string)$this->request->get('payment_amount'),
            'all' => $this->request->request->all()
        ];

        return $this;
    }

    /**
    * Get 3d Form Data
    *
    * @return array
    */
    public function get3DFormData(): array
    {
        $Token  = $this->create3DHash();
        $inputs = [
            'merchant_id'      =>  $this->account->merchant_id,
            'user_ip'          =>  isset($this->order->ip) ?$this->order->ip:$this->getIpAdress(),
            'merchant_oid'     =>  $this->order->id,
            'email'            =>  trim($this->order->email)?:'msn@msn.com',
            'payment_type'     =>  "card",
            'payment_amount'   =>  $this->amountFormat($this->order->amount),
            'currency'         =>  $this->order->currency,
            'test_mode'        =>  $this->mode=='TEST'?1:"0",
            'non_3d'           =>  $this->account->model != '3d' ? '1' : '0',
            'user_name'        =>  $this->order->name,
            'user_address'     =>  $this->order->address??"No Address",
            'user_phone'       =>  $this->order->phone??'05555555555',
            'user_basket'      =>  htmlentities(json_encode(($this->order->order_list??[]))),
            'debug_on'         =>  $this->mode=='TEST'?1:"0",
            'client_lang'      =>  isset($this->order->lang) ?strtolower($this->order->lang):"tr",
            'paytr_token'      => $Token,
            'card_type'        => $this->order->installment>1?$this->getCardType():"",
            'non3d_test_failed'=> $this->mode=='TEST'?1:"0",
            'installment_count'=> $this->order->installment>1 ?$this->order->installment:'0',
            'cc_owner'         => $this->card->name,
            'card_number'      => $this->card->number,
            'expiry_month'     => $this->card->month,
            'expiry_year'      => $this->card->year,
            'cvv'              => $this->card->cvv,
            'merchant_ok_url'  => $this->order->success_url,
            'merchant_fail_url'=> $this->order->fail_url
        ];

        return [
            'gateway'       => $this->url,
            'success_url'   => $this->order->success_url,
            'fail_url'      => $this->order->fail_url,
            'hash'          => $Token,
            'inputs'        => $inputs,
        ];
    }
    /**
    * Get 3d Form 
    *
    * @return mixed
    * @throws UnknownError
    */
    public function get3DForm(): string
    {
        $form_data = (array) $this->get3DFormData();
        $return = '<form method="post" action="'.$form_data['gateway'].'"  name="3dForm" class="redirect-form" role="form">';
        foreach ($form_data['inputs'] as $key => $value){
            $return .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
        }
        $return .= '<div class="text-center">Yönlendiriliyorsunuz...</div> <hr>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-lg btn-block btn-success">Ödeme Doğrulaması Yap</button>
            </div>
        </form>
        <script type="text/javascript">document.forms[0].submit();</script>';
        return $return;
    }
    /**
    * Get Card Type
    *
    * @return mixed
    * @throws UnknownError
    */
    public function getCardType(): string
    {
        $KartNumber = substr($this->card->number, 0, 8);
        if(!$KartNumber){
            return "";
        }
        $hash_str   = $KartNumber . $this->account->merchant_id . $this->account->merchant_salt;
        $paytr_token= base64_encode(hash_hmac('sha256', $hash_str, $this->account->merchant_key, true));
        $post_vals  = [
            'merchant_id'=>$this->account->merchant_id,
            'bin_number'=> $KartNumber,
            'paytr_token'=>$paytr_token
        ];
        ############################################################################################

        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL,  $this->url."/api/bin-detail");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1) ;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = @curl_exec($ch);
        if(curl_errno($ch)){
            return "";
        }
        curl_close($ch);
	
        $result= json_decode($result,1);
        if($result['status']=='success')
            return $result['brand']=='none'?'':$result['brand'];
       else 
            return "";
    }

    /**
     * Prepare Order
     *
     * @param object $order
     * @param object null $card
     * @return mixed
     * @throws UnsupportedTransactionTypeException
     */
    public function prepare($order, $card = null)
    {
        $this->type = $this->types['pay'];
        if (isset($order->transaction)) {
            if (array_key_exists($order->transaction, $this->types)) {
                $this->type = $this->types[$order->transaction];
            } else {
                throw new UnsupportedTransactionTypeException('Unsupported transaction type!');
            }
        }

        $this->order = $order;
        $this->card = $card;

        if ($this->card) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            if(strlen($this->card->year)==4)
            $this->card->year  = substr($this->card->year,-2,2);
        }
        if($this->order->currency==826){
            $this->order->currency  = "GBP";
        }elseif($this->order->currency==949){
            $this->order->currency  = "TL";
        }elseif($this->order->currency==840){
            $this->order->currency  = "USD";
        }elseif($this->order->currency==978){
            $this->order->currency  = "EUR";
        }

        $this->order->id =  preg_replace("/[^a-zA-Z0-9]+/", "", $this->order->id);
    }

    /**
     * Make Payment
     *
     * @param object $card
     * @return mixed
     * @throws UnsupportedPaymentModelException
     * @throws GuzzleException
     */
    public function payment($card)
    {
        $model = 'regular';
        if (isset($this->account->model) && $this->account->model) {
            $model = $this->account->model;
        }

        if ($model == 'regular' or $model == '3d') {
            $this->make3DPayment();
        } else {
            throw new UnsupportedPaymentModelException();
        }

        return $this;
    }

    /**
    * Refund Order
    *
    * @param $meta
    * @return $this
    */
    public function refund(array $meta): PayTrPos
    {
       
    }

    /**
    * Cancel Order
    *
    * @param array $meta
    * @return $this
    */
    public function cancel(array $meta): PayTrPos
    {
        
    }
    /**
    * Make 3D Pay Payment
    *
    * @return $this
    */
    public function make3DPayPayment(): PayTrPos
    {
    }
    /**
     * Regular Payment
     *
     * @return $this
     */
    public function makeRegularPayment(): PayTrPos
    {
    }
    /**
    * Order Status
    *
    * @param array $meta
    * @return $this
    */
    public function status(array $meta): PayTrPos
    {
    }

    /**
     * Send contents to WebService
     *
     * @param $contents
     * @return $this
     */
    public function send($contents): PayTrPos
    {
    }

    /**
    * Order History
    *
    * @param array $meta
    * @return $this
    */
    public function history(array $meta): PayTrPos
    {
    }

    /**
    * Get Installment List
    *
    * @param array $meta
    * @return $this
    */
    public function getInstallmentList(array $meta): PayTrPos
    {
		$request_id		= time();
		$paytr_token	= base64_encode(hash_hmac('sha256',$this->account->merchant_id.$request_id.$this->account->merchant_salt,$this->account->merchant_key,true));

		$post_vals		= ['merchant_id'=>$this->account->merchant_id,
			'request_id'=>$request_id,
			'paytr_token'=>$paytr_token,
			'single_ratio'=>$meta['isTekCekim']
		];

		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url."/taksit-oranlari");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1) ;
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 90);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 90);

		//XXX: DİKKAT: lokal makinanızda "SSL certificate problem: unable to get local issuer certificate" uyarısı alırsanız eğer
		//aşağıdaki kodu açıp deneyebilirsiniz. ANCAK, güvenlik nedeniyle sunucunuzda (gerçek ortamınızda) bu kodun kapalı kalması çok önemlidir!
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		$result = @curl_exec($ch);

		if(curl_errno($ch))
		{
			echo curl_error($ch);
			curl_close($ch);
			return [];
		}

		curl_close($ch);
		$result	=	json_decode($result,1);

		if($result['status']=='success'){
			$this->response = (object)[
				'max_inst_non_bus' => $result['max_inst_non_bus'],
				'data' => $result
			];

			return $this;
		}else {
			$this->response = (object)[
				'result' => $result,
				'max_inst_non_bus' => 0,
				'data' => null
			];
		}
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @return mixed
     */
    public function getOrder(): array
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function setCard($card=null)
    {
        return $this->card	= $card;
    }
	
    public function getCard()
    {
        return $this->card;
    }
}
