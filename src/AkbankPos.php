<?php

namespace Ankapix\SanalPos;

use GuzzleHttp\Exception\GuzzleException;
use Ankapix\SanalPos\Exceptions\UnsupportedPaymentModelException;
use Ankapix\SanalPos\Exceptions\UnsupportedTransactionTypeException;
use Ankapix\SanalPos\Exceptions\UnknownError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AkbankPos
 * @package Ankapix\SanalPos
 */
class AkbankPos implements PosInterface
{
    use PosHelpersTrait;

    /**
     * @const string
     */
    public const NAME = 'AkbankPay';

    /**
     * Response Codes
     *
     * @var array
     */
    public $codes = [
        "00" => "ONAY KODU XXXXXX",
        "01" => "BANKANIZI ARAYIN",
        "02" => "BANKANIZI ARAYIN",
        "04" => "RED-KARTA EL KOY",
        "05" => "RED-ONAYLANMADI",
        "08" => "KIMLIK KONTROLU",
        "11" => "ONAY KODU XXXXXX",
        "12" => "RED-GEÇERSİZ ISLEM",
        "13" => "RED-GEÇERSİZ TUTAR",
        "14" => "RED-HATALI KART",
        "15" => "RED-GEÇERSİZ KART",
        "19" => "TEKRAR DENEYIN",
        "33" => "RED-KARTA EL KOY",
        "34" => "RED-KARTA EL KOY",
        "38" => "RED-KARTA EL KOY",
        "41" => "RED-KARTA EL KOY",
        "43" => "RED-KARTA EL KOY",
        "51" => "YETERSIZ BAKIYE",
        "54" => "RED-ONAYLANMADI",
        "55" => "RED-SIFRE HATALI",
        "57" => "RED-ONAYLANMADI",
        "58" => "RED-GEÇERSİZ KART",
        "62" => "RED-ONAYLANMADI",
        "65" => "RED-BANKANIZI ARAYIN",
        "75" => "RED-ONAYLANMADI",
        "77" => "GÖNDERILEMEDI",
        "80" => "TAKSIT 50TL USTU",
        "81" => "BANKANIZI ARAYIN",
        "83" => "SISTEM HATASI",
        "85" => "SISTEM HATASI",
        "91" => "BANKANIZI ARAYIN",
        "92" => "RED-GEÇERSİZ KART",
        "93" => "SISTEM HATASI",
        "96" => "BANKANIZI ARAYIN",
        "20" => "TIC. LIMIT ASIMI",
        "03" => "BASARISIZ ISLEM",
        "07" => "KARTA EL KOYUNUZ",
        "56" => "SIFRE GIRINIZ 1",
        "57" => "SIFRE GIRINIZ 2",
        "58" => "TANIMSIZ KART",
        "30" => "BANKANIZI ARAYIN",
        "21" => "BASARISIZ ISLEM",
        "44" => "KARTA EL KOYUNUZ",
        "53" => "BAŞARISIZ İŞLEM",
        "61" => "YETERSIZ BAKIYE",
        "82" => "KART CVV HATASI",
        "99" => "BANKANIZI ARAYIN",
        "VPS-0000" => "BAŞARILI",
        "VPS-1000" => "TxnCode:{0} Hatalı İşlem Kodu",
        "VPS-1001" => "JsonSyntaxException Input format Error",
        "VPS-1002" => "Terminal:{0} bulunamadı",
        "VPS-1003" => "İşyeri:{0} bulunamadı",
        "VPS-1004" => "",
        "VPS-1005" => "{0} Provizyon Hatası",
        "VPS-1006" => "{0} Hatalı Tutar",
        "VPS-1007" => "Orjinal İşlem bulunamadı İşyeri :{0} Sipariş Numarası : {1}",
        "VPS-1008" => "Daha önce iptal edilmiş işlem",
        "VPS-1009" => "Şüpheli işlem",
        "VPS-1010" => "Reverse edilmiş işlem",
        "VPS-1011" => "Bilinmeyen işlem statüsü",
        "VPS-1012" => "Orjinal işlem başarılı değil",
        "VPS-1013" => "Sipariş Numarası : {0} tekil olmalıdır",
        "VPS-1014" => "Batch kapanmış, işlem iptal edilemez",
        "VPS-1015" => "Batch bulunamadı",
        "VPS-1016" => "Kart son kullanma tarihi geçersiz",
        "VPS-1017" => "Maksimum taksit sayısı hatalı",
        "VPS-1018" => "Kart Fraud maksimum hatalı işlem sayısı aşıldı",
        "VPS-1019" => "Hatalı para birimi",
        "VPS-1020" => "İade edilebilir tutar hatalı",
        "VPS-1021" => "Batch kapanmamış, iade yapılamaz, Lütfen iptal işlemi yapınız",
        "VPS-1022" => "Kapanmış önprovizyon",
        "VPS-1023" => "İade için orijinal işlem tipi hatalı",
        "VPS-1024" => "Önprovizyon açık değil",
        "VPS-1025" => "Önprovizyon kapama için orjinal işlem tipi hatalı",
        "VPS-1026" => "İptal için orjinal işlem tipi hatalı",
        "VPS-1027" => "Önprovizyon kısmi iptal tutar hatalı",
        "VPS-1028" => "İade edilmiş işlem iptal edilemez",
        "VPS-1029" => "Hatalı önprovizyon kapama tutarı",
        "VPS-1030" => "IP Fraud maksimum hatalı işlem sayısı aşıldı",
        "VPS-1031" => "Kart Fraud maksimum hatalı işlem sayısı aşıldı",
        "VPS-1032" => "İşyeri bazlı Ip kısıtı",
        "VPS-1033" => "Bin bazlı kısıtlama",
        "VPS-1034" => "Terminal yetki profili bulunamadı",
        "VPS-1035" => "Terminal işlem yetkisi bulunmamaktadır",
        "VPS-1037" => "Tek çekim kısıtı",
        "VPS-1038" => "Yurt dışı kart Non-Secure işlem Kısıtı",
        "VPS-1039" => "İşyeri MerchantSafeId/Secret Key bilgileri bulunamadı",
        "VPS-1040" => "İşyeri MerchantSafeId/Secret Key Erişim hatası",
        "VPS-1041" => "Hatalı currency exponent",
        "VPS-1042" => "Asıl işlem ile para birimleri uyuşmamaktadır",
        "VPS-1043" => "Sadece Akbank kartları chip-paralı işlemleri desteklemektedir",
        "VPS-1044" => "Not-Onus Kart hatalı taksit",
        "VPS-1045" => "MOTO işlem kısıtı",
        "VPS-1046" => "Satış iptal işleminde transaction alanı gönderilmemelidir.",
        "VPS-1047" => "Terminal bilgisi üye işyerine ait değildir. Terminal:{0}",
        "VPS-1048" => "Bu işyerinde yabancı kur desteklenmemektedir",
        "VPS-1049" => "Yurtiçi kartlarda yabancı kur desteklenmemektedir.",
        "VPS-1050" => "Not-Onus Kart ile Desteklenmeyen İşlem",
        "VPS-1051" => "Ön Provizyon İşleminde Chip-Para Kullanımı Desteklenmemektedir",
        "VPS-1052" => "Ortak ödeme input hash değerleri uyuşmamaktadır.",
        "VPS-1053" => "Kart Bilgisi bulunamamıştır.",
        "VPS-2000" => "İşyeri:{0} bulunamadı.",
        "VPS-2001" => "{0} input validasyon hatası.",
        "VPS-2002" => "Kayıt bulunmaktadır.",
        "VPS-2003" => "Terminal:{0} bulunamadı.",
        "VPS-2100" => "Şifreler uyuşmamaktadır.",
        "VPS-2101" => "Güvenlik Kurallarına Uymayan Şifre.Yeni girilen şifre büyük harf, küçük harf ve rakam içermelidir.",
        "VPS-2102" => "Girdiğiniz bilgileri kontrol ediniz",
        "VPS-2103" => "Şifre değiştirilemedi",
        "VPS-2104" => "Son 3 parolanızdan farklı bir parola belirleyiniz.",
        "VPS-2108" => "Bu sayfaya veya fonksiyona yetkiniz bulunmamaktadır.",
        "VPS-2109" => "İşleminize devam etmek için giriş yapınız.",
        "VPS-2110" => "Hatalı dosya formatı.",
        "VPS-2111" => "Dosya daha önce yüklenmiştir.",
        "VPS-2112" => "Hatalı dosya içeriği",
        "VPS-2120" => "Batch kaydı bulunmamaktadır.",
        "VPS-2121" => "Mutabakat kaydı bulunmamaktadır.",
        "VPS-2130" => "Girdiğiniz şifreye ait kayıt bulunamadı.",
        "VPS-2131" => "Girdiğiniz şifreyi kontrol ediniz.",
        "VPS-2132" => "Tek kullanımlık şifre gönderim limitine ulaştınız. Daha sonra tekrar deneyiniz.",
        "VPS-2133" => "Tek kullanımlık şifrenizi tekrar gönderiniz.",
        "VPS-2134" => "Tek kullanımlık şifre kullanılmıştır. Tekrar şifre gönderiniz.",
        "VPS-2137" => "Şifreniz blokelenmiştir. Devam etmek için şifremi unuttum adımından şifrenizi yenileyiniz.",
        "VPS-2138" => "Devam etmek için şifrenizi yenileyiniz.",
        "VPS-2139" => "Güvenlik resmi seçilmelidir.",
        "VPS-2140" => "Güvenlik kodunu doğrulayınız.",
        "VPS-2141" => "Güvenlik resmini doğrulayınız.",
        "VPS-2142" => "Güvenlik resmini doğrulayınız.",
        "VPS-2150" => "Mağazaya ait SecretKey bulunamadı.",
        "VPS-2151" => "Hash uyuşmamaktadır.",
        "VPS-2152" => "Hash üretilirken hata olmuştur.",
        "VPS-2153" => "Tpos dosya kaydı bulunamamıştır.",
        "VPS-2154" => "Dosya statusu geçersizdir.",
        "VPS-2155" => "Dosya kontrol edilmektedir.",
        "VPS-2170" => "İşleminiz gerçekleştrilemedi.",
        "VPS-2180" => "Kullanıcı bulunamamıştır.",
        "VPS-2190" => "Kart Bilgisi bulunamamıştır.",
        "VPS-2200" => "Terminal Yetki kaydı bulunamamıştır.",
        "VPS-2210" => "Kullanıcı ve Rol tipleri uyuşmamaktadır.",
        "VPS-2211" => "Kendi yetkinizi güncelleme izniniz bulunmamaktadır.",
        "VPS-1094" => "Link ile ödeme işlem yetkisi bulunmamaktadır",
        "VPS-1095" => "Link ile ödeme için girilen takip numarasına karşılık gelen kayıt bulunamadı",
        "VPS-1096" => "Link Oluşturma sürecinde api call üzerinden yapılan isteklerde Link Bilgilerini Alma isteğinin cevabında dönülen takip numarası girilmelidir.",
        "VPS-1097" => "Portal üzerinden yapılan link oluşturma isteklerinde payByLinkId alanının gönderimi zorunludur.",
        "VPS-1098" => "Linkteki token değerine karşılık gelen link kaydı bulunamadı",
        "VPS-1099" => "Linkin kullanım süresi dolmuştur, yeni link talep ederek işleminize devam ediniz",
        "VPS-1100" => "Link kilitlenmiştir, daha sonra tekrar deneyiniz",
        "VPS-1101" => "Link aktif değildir, yeni link talep ederek işleminize devam ediniz",
        "VPS-1102" => "Link ile ödeme deneme sayısı aşılmıştır, yeni link talep ederek işleminize devam ediniz",
        "VPS-1103" => "Girilen sipariş takip numarasına ait link kaydı bulunamadı",
        "VPS-1104" => "Link kaydının statüsü iptale uygun değildir",
        "VPS-1281" => "BKM 3DS Server İşlem Reddedildi",
        "VPS-1105" => "{0} Link ile Ödeme Hatalı Tutar",
        "VPS-3000" => "[DINAMIK_ALAN] alanı gönderilmesi gerekmektedir.",
        "VPS-3001" => "[DINAMIK_ALAN] alanı patterne uymamaktadır.",
        "VPS-3002" => "[DINAMIK_ALAN] hatalı bir alandır.",
        "VPS-3003" => "Geçersiz taksit değeri gönderilmiştir.",
        "VPS-3004" => "Geçersiz tutar gönderilmiştir.",
        "VPS-3005" => "Geçersiz işlem tipi gönderilmiştir",
        "VPS-3006" => "İşlem zaman aşımına uğramıştır.",
        "VPS-3007" => "Servis hatası gerçekleşmiştir.",
        "VPS-3008" => "Kullanıcı tarafından iptal edildi.",
        "VPS-8888" => "Güvenli Ödeme Doğrulaması Başarısız",
        "400" => "Yazım hatası veya eksik/hatalı alan gönderilmesi kaynaklı hata alınmaktadır.",
        "401" => "Hash hatası alınmaktadır. merchantSafeId, terminalSafeId, Secret Key ve hesapladığınız hash değerlerini kontrol ediniz.",
    ];


    /**
     * Transaction Types
     *
     * @var array
     */
    public $types = [
        'pay'   => '1',
    ];

    /**
     * Currencies
     *
     * @var array
     */
    public $currencies = [];

    /**
     * Transaction Type
     *
     * @var string
     */
    public $type;

    /**
     * API Account
     *
     * @var array
     */
    protected $account = [];

    /**
     * Order Details
     *
     * @var array
     */
    protected $order = [];

    /**
     * Credit Card
     *
     * @var object
     */
    protected $card;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response Raw Data
     *
     * @var object
     */
    protected $data;

    /**
     * Processed Response Data
     *
     * @var mixed
     */
    public $response;

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Mode
     *
     * @var string
     */
    protected $mode = 'PROD';

    /**
     * API version
     * @var string
     */
    protected $version = '1.00';

    /**
     * GarantiPost constructor.
     *
     * @param array $config
     * @param array $account
     * @param array $currencies
     */
    public function __construct($config, $account, array $currencies)
    {
        $request = Request::createFromGlobals();
        $this->request = $request->request;

        $this->config = $config;
        $this->account = $account;
        $this->currencies = $currencies;

        $this->url = isset($this->config['urls'][$this->account->env]) ?
            $this->config['urls'][$this->account->env] :
            $this->config['urls']['production'];

        $this->gateway = isset($this->config['urls']['gateway'][$this->account->env]) ?
            $this->config['urls']['gateway'][$this->account->env] :
            $this->config['urls']['gateway']['production'];

        if ($this->account->env == 'test') {
            $this->mode = 'TEST';
        }

        return $this;
    }

    /**
     * Amount Formatter
     *
     * @param double $amount
     * @return float
     */
    public function amountFormat($amount)
    {
        // Virgülleri noktaya çevir ve double olarak formatla
        $normalizedAmount = (double) str_replace(',', '.', $amount);
		
        // Her durumda iki ondalık basamak olacak şekilde formatla
        return number_format($normalizedAmount, 2, '.', '');
    }

    /**
     * Get ProcReturnCode
     *
     * @return string|null
     */
    protected function getProcReturnCode(): ?string
    {
        return isset($this->data->hostResponseCode) ? (string) $this->data->hostResponseCode : (isset($this->data->responseCode) ? (string) $this->data->responseCode : null);
    }

    /**
     * Get Status Detail Text
     *
     * @return string|null
     */
    protected function getStatusDetail(): ?string
    {
        $proc_return_code = $this->getProcReturnCode();

        return $proc_return_code ? (isset($this->codes[$proc_return_code]) ? (string) $this->codes[$proc_return_code] : null) : null;
    }


    /**
     * Create Regular Payment Post
     *
     * @return object
     */
    protected function createRegularPaymentPOST()
    {
        $json  = json_encode([
            "version" => $this->version,
            "txnCode" => "1000",
            "requestDateTime" => $this->formatDate(new \DateTime()),
            "randomNumber" => $this->getRandomNumberBase16(128),
            "terminal" => [
                "merchantSafeId" => $this->account->merchant_id,
                "terminalSafeId" => $this->account->terminal_id,
            ],
            "card" => [
                "cardHolderName" => $this->card->name,
                "cardNumber" => $this->card->number,
                "cvv2" => $this->card->cvv,
                "expireDate" => str_pad($this->card->month, 2, '0', STR_PAD_LEFT).substr($this->card->year, -2, 2),
            ],
            "reward" => [
                "ccbRewardAmount" => "0.00",
                "pcbRewardAmount" => "0.00",
                "xcbRewardAmount" => "0.00",
            ],
            "transaction" => [
                "amount" => (string) $this->amountFormat($this->order->amount),
                "currencyCode" =>  $this->order->currency,
                "motoInd" => 0,
                "installCount" => $this->order->installment>1?$this->order->installment:1,
            ],
            "customer" => [
                "emailAddress" => (string) filter_var($this->order->email, FILTER_VALIDATE_EMAIL)?$this->order->email:"msn@msn.com",
                "ipAddress" => isset($this->order->ip) ?$this->order->ip:$this->getIpAdress(),
            ],
        ]);
        $hash = $this->hashToString($json, $this->account->secret_key);
        return $this->sendRequest($this->url, $json, $hash);
    }
    
    /**
     * formatDate
     *
     * @param  mixed $date
     * @return void
     */
    protected function formatDate($date) {
        return $date->format('Y-m-d\TH:i:s.v');
    }    
    /**
     * hashToString
     *
     * @param  mixed $data
     * @param  mixed $secretKey
     * @return void
     */
    protected function hashToString($data, $secretKey) {
        return base64_encode(hash_hmac('sha512', $data, $secretKey, true));
    }  
    /**
     * hashToArray
     *
     * @param  mixed $data
     * @param  mixed $secretKey
     * @return void
     */
    protected function hashToArray($data, $secretKey) {
        return base64_encode(hash_hmac('sha512', implode('', $data), $secretKey, true));
    }   
    /**
     * checkResponseHash
     *
     * @param  mixed $requestMap
     * @param  mixed $secretKey
     * @return void
     */
    protected function checkResponseHash($requestMap, $secretKey){
        $params = explode("+", $requestMap["hashParams"]);
        $builder = "";
        foreach($params as $param){
            $builder .= $requestMap[$param];
        }
    
        $hash = $this->hashToString($builder, $secretKey);
        if($requestMap["hash"] !== $hash){
            return false;
        }
        return true;
    }
        
    /**
     * sendRequest
     *
     * @param  mixed $url
     * @param  mixed $json
     * @param  mixed $hash
     * @return void
     */
    function sendRequest($url, $json, $hash) {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'auth-hash: '.$hash,
                'Content-Type: application/json',
                'Accept: application/json'
            ),
            CURLOPT_POSTFIELDS => $json
        ));
        $response = curl_exec($ch);
        if($response === FALSE){
            die(curl_error($ch));
        }
        curl_close($ch);
        $this->data = json_decode($response);
        return $this;
    }    
    /**
     * getRandomNumberBase16
     *
     * @param  mixed $n
     * @return void
     */
    function getRandomNumberBase16 ($n)
    {
        $characters = '0123456789ABCDEF';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return strtoupper($randomString);
    }

    /**
     * Regular Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function makeRegularPayment()
    {
        $this->createRegularPaymentPOST();

        $status = 'declined';
        $response= 'Başarısız İşlem';
        if ($this->data->hostResponseCode == '00') {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object) [
            'id'                => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'reference_no'      => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'order_id'          => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'response'          => $status=='approved'?$response: ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'status'            => $status,
            'status_detail'     => ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'error_code'        => $status=='approved'?null: (isset($this->data->hostResponseCode) ? $this->printData($this->data->hostResponseCode) : (isset($this->data->responseCode) ? $this->printData($this->data->responseCode) : null)),
            'error_message'     => $status=='approved'?null: ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'txnDateTime'       => isset($this->data->txnDateTime) ? $this->data->txnDateTime : null,
            'all'               => $this->data
        ];

        return $this;
    }

    /**
     * Make 3D Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function make3DPayment()
    
    {
        $this->request  = Request::createFromGlobals();
        $this->create3DPaymentPOST();
        $status         = 'declined';
        $response       = 'Başarısız İşlem';
        
        if(!$this->checkResponseHash($this->request->request->all(), $this->account->secret_key)  or !in_array($this->request->get('mdStatus'), ["1", "4"])){
            $status = 'declined';
            $response = 'Başarısız İşlem';
        }elseif ($this->data->responseCode == 'VPS-0000' and in_array($this->request->get('mdStatus'), ["1", "4"])) {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object)[
            'id'                => isset($this->data->order->orderId) ? $this->printData($this->data->order->orderId) : null,
            'reference_no'      => isset($this->data->order->orderId) ? $this->printData($this->data->order->orderId) : null,
            'order_id'          => isset($this->data->order->orderId) ? $this->printData($this->data->order->orderId) : null,
            'response'          => $status=='approved'?$response:($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'status'            => $status,
            'status_detail'     => ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'error_code'        => $status=='approved'?null: (isset($this->data->hostResponseCode) ? $this->printData($this->data->hostResponseCode) : null),
            'error_message'     => $status=='approved'?null: ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'txnDateTime'       => isset($this->data->txnDateTime) ? $this->data->txnDateTime : null,
            'all'               => $this->data
        ];

        return $this;
    }

    /**
    * Get 3d Form Data
    *
    * @return array
    */
    public function get3DFormData()
    {
        $inputs = [
            'paymentModel'  => $this->account->model == '3d_pay' ? '3D_PAY' : '3D',
            "txnCode"       => "3000",
            "merchantSafeId" => $this->account->merchant_id,
            "terminalSafeId" => $this->account->terminal_id,
            "orderId"       => $this->order->id,
            "lang"          => "TR",
            "amount"        => (string) $this->amountFormat($this->order->amount),
            "ccbRewardAmount" => "0.00",
            "pcbRewardAmount" => "0.00",
            "xcbRewardAmount" => "0.00",
            "currencyCode" =>  $this->order->currency,
            "installCount" => $this->order->installment>1?$this->order->installment:1,
            "okUrl" =>  $this->order->success_url,
            "failUrl" =>  $this->order->fail_url,
            "emailAddress" => (string) filter_var($this->order->email, FILTER_VALIDATE_EMAIL)?$this->order->email:"msn@msn.com",
            "creditCard" => $this->card->number,
            "expiredDate" => str_pad($this->card->month, 2, '0', STR_PAD_LEFT).substr($this->card->year, -2, 2),
            "cvv" => $this->card->cvv,
            "randomNumber" => $this->getRandomNumberBase16(128),
            "requestDateTime" => $this->formatDate(new \DateTime())
        ];
        $hash = $this->hashToArray($inputs, $this->account->secret_key);
        $inputs['hash']     = $hash;
        return [
            'gateway'       => $this->gateway,
            'success_url'   => $this->order->success_url,
            'fail_url'      => $this->order->fail_url,
            'hash'          => $hash,
            'inputs'        => $inputs,
        ];
    }
    /**
    * Get 3d Form 
    *
    * @return mixed
    * @throws UnknownError
    */
    public function get3DForm()
    {
        $form_data = (array) $this->get3DFormData();
        $return = '<form method="post" action="'.$form_data['gateway'].'"  name="3dForm" class="redirect-form" role="form">';
        foreach ($form_data['inputs'] as $key => $value){
            $return .= '<input type="hidden" name="'.$key.'" value="'.$value.'">';
        }
        $return .= '<div class="text-center">Yönlendiriliyorsunuz...</div> <hr>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-lg btn-block btn-success">Ödeme Doğrulaması Yap</button>
            </div>
        </form>
        <script type="text/javascript">document.forms[0].submit();</script>';
        return $return;
    }

    /**
     * Prepare Order
     *
     * @param object $order
     * @param object null $card
     * @return mixed
     * @throws UnsupportedTransactionTypeException
     */
    public function prepare($order, $card = null)
    {
        $this->type = $this->types['pay'];
        if (isset($order->transaction)) {
            if (array_key_exists($order->transaction, $this->types)) {
                $this->type = $this->types[$order->transaction];
            } else {
                throw new UnsupportedTransactionTypeException('Unsupported transaction type!');
            }
        }

        $this->order = $order;
        $this->card = $card;

        if ($this->card) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            if(strlen($this->card->year)==4)
            $this->card->year  = substr($this->card->year,-2,2);
        }
    }

    /**
     * Make Payment
     *
     * @param object $card
     * @return mixed
     * @throws UnsupportedPaymentModelException
     * @throws GuzzleException
     */
    public function payment($card)
    {
        $this->card = $card;
        if (count((array) $card)) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            $this->card->year  = str_pad($this->card->year, 4, '20', STR_PAD_LEFT);
        }

        $model = 'regular';
        if (isset($this->account->model) && $this->account->model) {
            $model = $this->account->model;
        }

        if ($model == 'regular') {
            $this->makeRegularPayment();
        } elseif ($model == '3d') {
            $this->make3DPayment();
        } elseif ($model == '3d_pay') {
            $this->make3DPayPayment();
        } else {
            throw new UnsupportedPaymentModelException();
        }

        return $this;
    }

    /**
    * Refund Order
    *
    * @param $meta
    * @return $this
    * @throws GuzzleException
    */
    public function refund(array $meta)
    {
        return $this;
    }

    /**
    * Cancel Order
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function cancel(array $meta)
    {
        return $this;
    }

    /**
     * Create 3D Payment POST
     * @return object
     */
    protected function create3DPaymentPOST()
    {
        $json = json_encode([
            "version" => $this->version,
            "txnCode" => "1000",
            "requestDateTime" => $this->formatDate(new \DateTime()),
            "randomNumber" => $this->getRandomNumberBase16(128),
            "terminal" => [
                "merchantSafeId" => $this->account->merchant_id,
                "terminalSafeId" => $this->account->terminal_id,
            ],
            "order"=> [
                "orderId"=> $this->request->request->get("orderId")
            ],
            "transaction"=> [
                "amount"=> (string) $this->amountFormat($this->order->amount),
                "currencyCode"=> $this->order->currency,
                "motoInd"=> 0,
                "installCount"=>  $this->order->installment>1?$this->order->installment:1,
            ],
            "secureTransaction"=> [
                "secureId"=> $this->request->request->get("secureId"),
                "secureEcomInd"=> $this->request->request->get("secureEcomInd"),
                "secureData"=> $this->request->request->get("secureData"),
                "secureMd"=> $this->request->request->get("secureMd"),
            ]
        ]);

        $hash        = $this->hashToString($json, $this->account->secret_key);
        return $this->sendRequest($this->url, $json, $hash);
    }
    /**
    * Make 3D Pay Payment
    *
    * @return $this
    */
    public function make3DPayPayment()
    {
        $this->request  = Request::createFromGlobals();
        $this->create3DPaymentPOST();
        $status         = 'declined';
        $response       = 'Başarısız İşlem';

        if(!$this->checkResponseHash($this->request->request->all(), $this->account->secret_key)){
            $status = 'declined';
            $response = 'Başarısız İşlem';
        }elseif ($this->data->hostResponseCode == '00') {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object)[
            'id'                => isset($this->data->order->orderId) ? $this->printData($this->data->order->orderId) : null,
            'reference_no'      => isset($this->data->order->orderId) ? $this->printData($this->data->order->orderId) : null,
            'order_id'          => isset($this->data->order->orderId) ? $this->printData($this->data->order->orderId) : null,
            'response'          => $status=='approved'?$response:($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'status'            => $status,
            'status_detail'     => ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'error_code'        => $status=='approved'?null: (isset($this->data->hostResponseCode) ? $this->printData($this->data->hostResponseCode) : (isset($this->data->responseCode) ? $this->printData($this->data->responseCode) : null)),
            'error_message'     => $status=='approved'?null: ($this->getStatusDetail()?$this->getStatusDetail():$this->printData($this->data->responseMessage)),
            'txnDateTime'       => isset($this->data->txnDateTime) ? $this->data->txnDateTime : null,
            'all'               => $this->data
        ];

        return $this;
    }

    /**
    * Order Status
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function status(array $meta)
    {
    }

    /**
    * Send contents to WebService
    *
    * @param $contents
    * @return $this
    * @throws GuzzleException
    */
    public function send($contents)
    {
    }

    /**
    * Order History
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function history(array $meta)
    {
        return $this;
    }

    /**
    * Installment List
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function getInstallmentList(array $meta)
    {
        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
    * setCard
    *
    * @return $this
    */
    public function setCard($card=null)
    {
    }
}
