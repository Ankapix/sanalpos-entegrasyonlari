<?php

namespace Ankapix\SanalPos;

use GuzzleHttp\Exception\GuzzleException;
use Ankapix\SanalPos\Exceptions\UnsupportedPaymentModelException;
use Ankapix\SanalPos\Exceptions\UnsupportedTransactionTypeException;
use Ankapix\SanalPos\Exceptions\UnknownError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class IParaPos
 * @package Ankapix\SanalPos
 */
class IParaPos implements PosInterface
{
    use PosHelpersTrait;

    /**
     * @const string
     */
    public const NAME = 'IParaPay';

    /**
     * Response Codes
     *
     * @var array
     */
    public $codes = [
        '0'    => "Entegrasyon Hatası", // Entegrasyon hatası
        '1'    => "Ödeme başarılı",     // Ödeme başarılı
        '2'    => "Ödeme iade edilmiş", // Ödeme iade edilmiş
        '3'    => "Ödeme reddedilmiş",  // Ödeme reddedilmiş
        '4'    => "Ödeme bulunamadı",   // Ödeme bulunamadı
        '6'    => "Parçalı iade talebinde bulunulmuş ancak iPara tarafından manuel iade yapılması gerekiyor", 
        '7'    => "İade talebinde bulunulmuş ancak iPara tarafından manuel iade yapılması gerekiyor", 
        '8'    => "İade talebinde bulunulmuş ancak Mağaza’dan geri para transferi yapılması gerekiyor", 
        '9'    => "Parçalı iade talebinde bulunulmuş ancak Mağaza’dan geri para transferi yapılması gerekiyor"
    ];


    /**
     * Transaction Types
     *
     * @var array
     */
    public $types = [
        'pay'   => '1',
        'pre'   => '3'
    ];

    /**
     * Currencies
     *
     * @var array
     */
    public $currencies = [];

    /**
     * Transaction Type
     *
     * @var string
     */
    public $type;

    /**
     * API Account
     *
     * @var array
     */
    protected $account = [];

    /**
     * Order Details
     *
     * @var array
     */
    protected $order = [];

    /**
     * Credit Card
     *
     * @var object
     */
    protected $card;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response Raw Data
     *
     * @var object
     */
    protected $data;

    /**
     * Processed Response Data
     *
     * @var mixed
     */
    public $response;

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Mode
     *
     * @var string
     */
    protected $mode = 'PROD';

    /**
     * API version
     * @var string
     */
    protected $version = 'V3.4';

    /**
     * GarantiPost constructor.
     *
     * @param array $config
     * @param array $account
     * @param array $currencies
     */
    public function __construct($config, $account, array $currencies)
    {
        $request = Request::createFromGlobals();
        $this->request = $request->request;

        $this->config = $config;
        $this->account = $account;
        $this->currencies = $currencies;

        if ($this->account->env == 'test') {
            $this->mode = 'TEST';
        }

        return $this;
    }

    /**
     * Amount Formatter
     *
     * @param double $amount
     * @return float
     */
    public function amountFormat($amount)
    {
        // Virgül varsa noktaya çeviriyoruz
        $amount = str_replace(',', '.', $amount);
    
        // Tutarı kuruşlu haliyle (100 ile çarpılarak) tam sayıya çeviriyoruz
        return (int) round($amount * 100);
    }


    /**
     * Create Regular Payment Post
     *
     * @return object
     */
    protected function createRegularPaymentPOST()
    {
        $settings            = new \IPara\Settings();
        $settings->PublicKey = $this->account->public_key;
        $settings->PrivateKey= $this->account->private_key;
        $settings->Mode      = $this->mode=='TEST'?"T":"P";

        // iPara API client oluşturma
        $request    = new \IPara\ApiPaymentRequest();
        $request->OrderId = $this->order->id;
        $request->Echo = "uludem.com.tr";
        $request->Mode  = $this->mode == 'TEST' ? 'T' : 'P'; // Test veya prod modu
        $request->Amount = (string) $this->amountFormat($this->order->amount); // Tutarı formatlayın
        $request->CardOwnerName =  $this->card->name;
        $request->CardNumber =  $this->card->number;
        $request->CardExpireMonth = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
        $request->CardExpireYear = substr($this->card->year, -2, 2); // Son iki haneyi kullanıyoruz
        $request->Installment = $this->order->installment>1?$this->order->installment:1;
        $request->Cvc = $this->card->cvv;
        $request->ThreeD = "false";

        #region Sipariş veren bilgileri
        if (is_null($request->Purchaser)) {
            $request->Purchaser = new \IPara\Purchaser();
        }
        $nameParts = explode(' ', trim($this->card->name)); // İsmi boşluklardan böl
        // Son kelimeyi soyadı olarak al
        $surname = array_pop($nameParts);

        // Geriye kalanları birleştirip adı oluştur
        $name = implode(' ', $nameParts);

        // Değerleri ata
        $request->Purchaser->Name = $name;
        $request->Purchaser->Surname = $surname;
        $request->Purchaser->BirthDate = "1986-07-11";
        $request->Purchaser->Email = (string) filter_var($this->order->email, FILTER_VALIDATE_EMAIL)?$this->order->email:"msn@msn.com";
        $request->Purchaser->ClientIp = isset($this->order->ip) ?$this->order->ip:$this->getIpAdress();
        #endregion

        #region Ürün bilgileri
        $request->Products =  [];
        foreach($this->order->order_list as $Bas){
            $basketItem = new \IPara\Product();
            $basketItem->Title   = $Bas['Title'];
            $basketItem->Code    = $Bas['Code'];
            $basketItem->Price   = $Bas['Price'];
            $basketItem->Quantity = $Bas['Quantity'];
            $request->Products[] = $basketItem;
        }
        
        $output       =  \IPara\ApiPaymentRequest::execute($request,$settings); 
        $xml_response = new \SimpleXMLElement($output);
        if ($xml_response == NULL) {
            throw new \Exception("Ödeme cevabı xml formatında değil");
        }
        $this->data         = $xml_response;
        #endregion
        return $this;
    }
    /**
     * Create 3D Payment POST
     * @return object
     */
    protected function create3DPaymentPOST()
    {
        $settings            = new \IPara\Settings();
        $settings->PublicKey = $this->account->public_key;
        $settings->PrivateKey= $this->account->private_key;
        $settings->Mode      = $this->mode=='TEST'?"T":"P";
        $request 			= new \IPara\PaymentInquiryRequest();
        $request->orderId 	= (string) $this->request->get('orderId');
        $request->Echo 		= "uludem.com.tr";
        $output             =  \IPara\PaymentInquiryRequest::execute($request, $settings); 
        $xml_response       = new \SimpleXMLElement($output);
        if ($xml_response == NULL) {
            throw new \Exception("Ödeme cevabı xml formatında değil");
        }
        // SimpleXMLElement'i objeye dönüştür
        $object = json_decode(json_encode($xml_response));
        $this->data         = $object;
        return $this;
    }

    /**
     * Regular Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function makeRegularPayment()
    {
        $this->createRegularPaymentPOST();
        $status = 'declined';
        $response= 'Başarısız İşlem';
        if (isset($this->data->result) and $this->data->result == 1) {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object) [
            'id'                => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'reference_no'      => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'order_id'          => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'response'          => $status=='approved'?$response:(isset($this->data->errorMessage) ? $this->printData($this->data->errorMessage):null),
            'status'            => $status,
            'error_code'        => isset($this->data->errorCode) ? $this->printData($this->data->errorCode) : null,
            'error_message'     => isset($this->data->errorMessage) ? $this->printData($this->data->errorMessage) : null,
            'echo'              => isset($this->data->echo) ? $this->data->echo : null,
            'transactionDate'   => isset($this->data->transactionDate) ? $this->data->transactionDate : null,
            'amount'            => isset($this->data->amount) ? $this->data->amount : null,
            'hash'              => isset($this->data->hash) ? $this->data->hash : null,
            'all'               => $this->data
        ];

        return $this;
    }

    /**
     * Make 3D Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function make3DPayment()
    
    {
        $this->create3DPaymentPOST();
        $status = 'declined';
        $response= 'Başarısız İşlem';
        if (isset($this->data->result) and $this->data->result == 1 and isset($this->data->responseMessage) and $this->data->responseMessage == "APPROVED") {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object) [
            'id'                => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'reference_no'      => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'order_id'          => isset($this->data->orderId) ? $this->printData($this->data->orderId) : null,
            'response'          => $status=='approved'?$response:(isset($this->data->errorMessage) ? $this->printData($this->data->errorMessage):null),
            'status'            => $status,
            'error_code'        => isset($this->data->errorCode) ? $this->printData($this->data->errorCode) : null,
            'error_message'     => isset($this->data->errorMessage) ? $this->printData($this->data->errorMessage) : null,
            'echo'              => isset($this->data->echo) ? $this->data->echo : null,
            'transactionDate'   => isset($this->data->transactionDate) ? $this->data->transactionDate : null,
            'amount'            => isset($this->data->amount) ? $this->data->amount : null,
            'hash'              => isset($this->data->hash) ? $this->data->hash : null,
            'all'               => $this->data,
            'post'              => $this->request->all()
        ];
        return $this;
    }

    /**
    * Get 3d Form Data
    *
    * @return array
    */
    public function get3DFormData()
    {
        $settings            = new \IPara\Settings();
        $settings->PublicKey = $this->account->public_key;
        $settings->PrivateKey= $this->account->private_key;
        $settings->Mode      = $this->mode=='TEST'?"T":"P";

        // iPara API client oluşturma
        $request    = new \IPara\Api3DPaymentRequest();
        $request->OrderId = $this->order->id;
        $request->Echo = "uludem.com.tr";
        $request->Mode  = $this->mode == 'TEST' ? 'T' : 'P'; // Test veya prod modu
        $request->Amount = (string) $this->amountFormat($this->order->amount); // Tutarı formatlayın
        $request->CardOwnerName =  $this->card->name;
        $request->CardNumber =  $this->card->number;
        $request->CardExpireMonth = $this->card->month;
        $request->CardExpireYear = $this->card->year; // Son iki haneyi kullanıyoruz
        $request->Installment = $this->order->installment>1?$this->order->installment:1;
        $request->Cvc = $this->card->cvv;
        $request->SuccessUrl = $this->order->success_url;
        $request->FailUrl = $this->order->fail_url;
        $request->ThreeD = "true";

        #region Sipariş veren bilgileri

        if (is_null($request->Purchaser)) {
            $request->Purchaser = new \IPara\Purchaser();
        }
        $nameParts = explode(' ', trim($this->card->name)); // İsmi boşluklardan böl
        // Son kelimeyi soyadı olarak al
        $surname = array_pop($nameParts);

        // Geriye kalanları birleştirip adı oluştur
        $name = implode(' ', $nameParts);

        // Değerleri ata
        $request->Purchaser->Name = $name;
        $request->Purchaser->Surname = $surname;
        $request->Purchaser->BirthDate = "1986-07-11";
        $request->Purchaser->Email = (string) filter_var($this->order->email, FILTER_VALIDATE_EMAIL)?$this->order->email:"msn@msn.com";
        $request->Purchaser->ClientIp = isset($this->order->ip) ?$this->order->ip:$this->getIpAdress();
        #endregion

        #region Ürün bilgileri
        $request->Products =  [];
        foreach($this->order->order_list as $Bas){
            $basketItem = new \IPara\Product();
            $basketItem->Title   = $Bas['Title'];
            $basketItem->Code    = $Bas['Code'];
            $basketItem->Price   = $Bas['Price'];
            $basketItem->Quantity = $Bas['Quantity'];
            $request->Products[] = $basketItem;
        }

        $response           = $request->execute3D($settings); 
        $this->data         = $response;
        #endregion
        return $this;
    }
    /**
    * Get 3d Form 
    *
    * @return mixed
    * @throws UnknownError
    */
    public function get3DForm()
    {
        $this->get3DFormData();
        return $this->data;
    }

    /**
     * Prepare Order
     *
     * @param object $order
     * @param object null $card
     * @return mixed
     * @throws UnsupportedTransactionTypeException
     */
    public function prepare($order, $card = null)
    {
        $this->type = $this->types['pay'];
        if (isset($order->transaction)) {
            if (array_key_exists($order->transaction, $this->types)) {
                $this->type = $this->types[$order->transaction];
            } else {
                throw new UnsupportedTransactionTypeException('Unsupported transaction type!');
            }
        }

        $this->order = $order;
        $this->card = $card;

        if ($this->card) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            if(strlen($this->card->year)==4)
            $this->card->year  = substr($this->card->year,-2,2);
        }
    }

    /**
     * Make Payment
     *
     * @param object $card
     * @return mixed
     * @throws UnsupportedPaymentModelException
     * @throws GuzzleException
     */
    public function payment($card)
    {
        $this->card = $card;
        if (count((array) $card)) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            $this->card->year  = str_pad($this->card->year, 4, '20', STR_PAD_LEFT);
        }

        $model = 'regular';
        if (isset($this->account->model) && $this->account->model) {
            $model = $this->account->model;
        }

        if ($model == 'regular') {
            $this->makeRegularPayment();
        } elseif ($model == '3d') {
            $this->make3DPayment();
        } else {
            throw new UnsupportedPaymentModelException();
        }

        return $this;
    }

    /**
    * Refund Order
    *
    * @param $meta
    * @return $this
    * @throws GuzzleException
    */
    public function refund(array $meta)
    {
        return $this;
    }

    /**
    * Cancel Order
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function cancel(array $meta)
    {
        return $this;
    }

    /**
    * Make 3D Pay Payment
    *
    * @return $this
    */
    public function make3DPayPayment()
    {
    }

    /**
    * Order Status
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function status(array $meta)
    {
    }

    /**
    * Send contents to WebService
    *
    * @param $contents
    * @return $this
    * @throws GuzzleException
    */
    public function send($contents)
    {
    }

    /**
    * Order History
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function history(array $meta)
    {
        return $this;
    }

    /**
    * Installment List
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function getInstallmentList(array $meta)
    {
        $settings            = new \IPara\Settings();
        $settings->PublicKey = $this->account->public_key;
        $settings->PrivateKey= $this->account->private_key;
        $settings->Mode      = $this->mode=='TEST'?"T":"P";
        //request
        $request            = new \IPara\BinNumberInquiryRequest();
        $request->binNumber = $meta['number'];
        $request->amount    = (double) $this->amountFormat(isset($meta['amount']) ? $meta['amount'] : 0);
        $request->threeD    = $this->account->model=='3d'?'true':'false';
        $output             = \IPara\BinNumberInquiryRequest::execute($request, $settings);
        $xml_response       = new \SimpleXMLElement($output);
        if ($xml_response == NULL) {
            throw new \Exception("Ödeme cevabı xml formatında değil");
        }
        $this->response = (object) [
            'data'      => isset($xml_response->installmentDetail) ? $xml_response->installmentDetail : null,
            'result'    => isset($xml_response->errorMessage) ? $xml_response->errorMessage : null, 
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
    * setCard
    *
    * @return $this
    */
    public function setCard($card=null)
    {
    }
}
