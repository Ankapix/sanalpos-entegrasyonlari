<?php

namespace Ankapix\SanalPos;

use GuzzleHttp\Exception\GuzzleException;
use Ankapix\SanalPos\Exceptions\UnsupportedPaymentModelException;
use Ankapix\SanalPos\Exceptions\UnsupportedTransactionTypeException;
use Ankapix\SanalPos\Exceptions\UnknownError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class QnbPayPos
 * @package Ankapix\SanalPos
 */
class QnbPayPos implements PosInterface
{
    use PosHelpersTrait;

    /**
     * @const string
     */
    public const NAME = 'QnbPayPos';

    /**
     * API URL
     *
     * @var string
     */
    public $url;
    /**
     * Response Codes
     *
     * @var array
     */
    public $codes = [
        '1'    => "Temel doğrulama",
        '3'    => "Fatura kimliği çoktan işlendi. Bu fatura kimliğine sahip siparişin işlem devam ediyor, lütfen bekleyin veya yeni fatura kimliğiyle sipariş oluşturun.",
        '12'    => "Öğeler bir dizi olmalı, Geçersiz Para Birimi kodu, Geçersiz öğe biçimi.",
        '13'    => "Ürün fiyatınızın toplamı (5), fatura toplamına (10) eşit değil.",
        '14'    => "Üye işyeri bulunamadı!",
        '30'    => "Geçersiz kimlik bilgileri",
        '31'    => "İşlem bulunamadı",
        '32'    => "Geçersiz fatura kimliği, sipariş tamamlanmadı.",
        '33'    => "Miktar tam sayı olmalıdır.",
        '34'    => "Ödeme entegrasyon yöntemine izin verilmiyor. Lütfen desteğe başvurunuz.",
        '35'    => "Kredi Kartı Ödeme Seçeneği tanımlanmadı.",
        '36'    => "Pos bulunamadı.",
        '37'    => "Üye işyeri Pos Komisyonu ayarlanmadı. Lütfen servis sağlayıcı ile iletişime geçiniz.",
        '38'    => "Bu para birimi ve ödeme yöntemi için Üye İşyeri Komisyonu belirlenmedi. Lütfen başka bir ödeme yöntemi deneyiniz.",
        '39'    => "Komisyon bulunamadı.",
        '40'    => "Taksit bulunamadı.",
        '41'    => "Ödeme başarısız",
        '42'    => "Ürün fiyatı komisyondan az, Ürün fiyatı maliyetten düşük",
        '43'    => "Ödeme vadesi ayarlanmamış",
        '44'    => "Bu kredi kartı bloke edilmiştir.",
        '45'    => "Üye İşyeri günlük işlem tutarı sınırı aşıldı",
        '46'    => "Üye İşyeri aylık işlem sayısı sınırı aşıldı",
        '47'    => "Üye İşyeri aylık işlem tutarı sınırı aşıldı",
        '48'    => "İşlem başına minimum işlem limiti ihlal edildi",
        '49'    => "İade Başarısız."
    ];

    /**
     * Transaction Types
     *
     * @var array
     */
    public $types = [
        'pay'   => '1'
    ];

    /**
     * Currencies
     *
     * @var array
     */
    public $currencies = [];

    /**
     * Transaction Type
     *
     * @var string
     */
    public $type;

    /**
     * API Account
     *
     * @var array
     */
    protected $account = [];

    /**
     * Order Details
     *
     * @var array
     */
    protected $order = ["currency"=>"TRY"];

    /**
     * Credit Card
     *
     * @var object
     */
    protected $card;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response Raw Data
     *
     * @var object
     */
    protected $data;

    /**
     * Processed Response Data
     *
     * @var mixed
     */
    public $response;

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Mode
     *
     * @var string
     */
    protected $mode = 'PROD';

    /**
     * API version
     * @var string
     */
    protected $version = 'V1.0';

    /**
     * GarantiPost constructor.
     *
     * @param array $config
     * @param array $account
     * @param array $currencies
     */
    public function __construct($config, $account, array $currencies)
    {
        $request = Request::createFromGlobals();
        $this->request = $request->request;

        $this->config = $config;
        $this->account = $account;
        $this->currencies = $currencies;

        $this->url = isset($this->config['urls'][$this->account->env]) ?
            $this->config['urls'][$this->account->env] :
            $this->config['urls']['production'];

        if ($this->account->env == 'test') {
            $this->mode = 'TEST';
        }

        return $this;
    }

    /**
     * Amount Formatter
     *
     * @param double $amount
     * @return float
     */
    public function amountFormat($amount)
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * Get getToken
     *
     * @return string|null
     */
    protected function getToken()
    {
        $IsLive     = $this->mode=='TEST'?false:true;
        $array = [
            'app_id' => $this->account->api_key,
            'app_secret' => $this->account->api_sec
        ];

        $environment_url = $IsLive ? 'https://portal.qnbpay.com.tr/ccpayment/api/token' : 'https://test.qnbpay.com.tr/ccpayment/api/token';
        return $this->getCurl($environment_url, 'POST', $array);
    }

    protected function getCurl($url, $method, $array, $header=[]){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $array,
            ));
    
            $response = curl_exec($curl);
    
            curl_close($curl);
            return  json_decode($response);
    }
    /**
     * Create 3D Hash
     *
     * @return string
     */
    public function create3DHash($invoice_id, $merchant_key, $app_secret) {
        $data = $invoice_id.'|'.$merchant_key;
        $iv = substr(sha1(mt_rand()), 0, 16);
        $password = sha1($app_secret);
        $salt = substr(sha1(mt_rand()), 0, 4);
        $saltWithPassword = hash('sha256', $password . $salt);
        $encrypted = openssl_encrypt(
            "$data", 'aes-256-cbc', "$saltWithPassword", null, $iv
        );
        $msg_encrypted_bundle = "$iv:$salt:$encrypted";
        $hash_key = str_replace('/', '__', $msg_encrypted_bundle);
        return $hash_key;
    }
    /**
     * Make 3D Payment
     *
     * @return $this
     */
    public function make3DPayment(): QnbPayPos

    {
        $this->request  = Request::createFromGlobals();
        $this->data     = $this->request->request->all();
        $status         = 'declined';
        
        if($this->request->get('payment_status')==1 and $this->request->get('invoice_id')){
            $status = $this->checkStatus($this->request->get('invoice_id'));
            if($status->status_code == 100 || $status->status_code == 69){
                $status = 'approved';
            }
        }elseif ($this->request->get('status') == 'success') {
            $status = 'declined';
        }

        $this->response = (object)[
            'id'                => $this->request->get('invoice_id'),
            'reference_no'      => $this->request->get('order_no'),
            'order_id'          => $this->request->get('order_id'),
            'response'          => $this->request->get('status_description') ? $this->request->get('status_description') : $this->request->get('error') ?$this->request->get('error') : null,
            'transaction_type'  => $this->type,
            'code'              => $this->request->get('status_code'),
            'status'            => $status,
            'error_code'        => $this->request->get('error_code'),
            'error_message'     => $this->request->get('error'),
            'comission'         => $this->request->get('user_commission'),
            'all'               => $this->data,
            'original'          => $this->request
        ];

        return $this;
    }

    /**
    * Get 3d Form Data
    *
    * @return array
    */
    public function get3DFormData(): array
    {
        $IsLive     = $this->mode=='TEST'?false:true;
        $token 		= $this->getToken()->data;
        $environment_url = $IsLive ? 'https://portal.qnbpay.com.tr/ccpayment/api/paySmart3D' : 'https://test.qnbpay.com.tr/ccpayment/api/paySmart3D';
        $inputs = [
            "cc_holder_name" => $this->card->name,
            "cc_no" => $this->card->number,
            "expiry_month" => $this->card->month,
            "expiry_year" => $this->card->year,
            "cvv" => $this->card->cvv,
            "currency_code" => $this->order->currency,
            "installments_number" => $this->order->installment>1 ?$this->order->installment:1,
            "invoice_id" => $this->order->id,
            "invoice_description" => $this->order->id." Nolu ödeme",
            "total" => $this->amountFormat($this->order->amount),
            "merchant_key" => $this->account->merchant_key,
            "items" => json_encode($this->order->order_list??[]),
            "name" => $this->order->name,
            "surname" => $this->order->name,
            "is_comission_from_user"=>$this->account->vade_fark?"1":"0",
            "commission_by"=>$this->account->vade_fark?"user":"",
            "hash_key" =>$this->generateHashKey($this->amountFormat($this->order->amount),($this->order->installment>1 ?$this->order->installment:1), $this->order->currency, $this->account->merchant_key, $this->order->id, $this->account->api_sec),
            'return_url'  => $this->order->success_url,
            'cancel_url'=> $this->order->fail_url
        ];
	
        return [
            'gateway'       => $environment_url,
            'success_url'   => $this->order->success_url,
            'fail_url'      => $this->order->fail_url,
            'inputs'        => $inputs,
        ];
    }
    /**
    * Get 3d Form 
    *
    * @return mixed
    * @throws UnknownError
    */
    public function get3DForm(): string
    {
        $form_data = (array) $this->get3DFormData();
        $return = '<form method="post" action="'.$form_data['gateway'].'"  name="3dForm" class="redirect-form" role="form">';
        foreach ($form_data['inputs'] as $key => $item){
            $return .=  "<input type='hidden' name='{$key}' value='".$item. "'>";
        }
        $return .= '<div class="text-center">Yönlendiriliyorsunuz...</div> <hr>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-lg btn-block btn-success">Ödeme Doğrulaması Yap</button>
            </div>
        </form>
        <script type="text/javascript">document.forms[0].submit();</script>';
        return $return;
    }

    /**
     * Prepare Order
     *
     * @param object $order
     * @param object null $card
     * @return mixed
     * @throws UnsupportedTransactionTypeException
     */
    public function prepare($order, $card = null)
    {
        $this->type = $this->types['pay'];
        if (isset($order->transaction)) {
            if (array_key_exists($order->transaction, $this->types)) {
                $this->type = $this->types[$order->transaction];
            } else {
                throw new UnsupportedTransactionTypeException('Unsupported transaction type!');
            }
        }

        $this->order = $order;
        $this->card = $card;

        if ($this->card) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
        }
        if($this->order->currency==949){
            $this->order->currency  = "TRY";
        }elseif($this->order->currency==840){
            $this->order->currency  = "USD";
        }elseif($this->order->currency==978){
            $this->order->currency  = "EUR";
        }else{
            $this->order->currency  = "TRY";
        }
    }

    /**
     * Make Payment
     *
     * @param object $card
     * @return mixed
     * @throws UnsupportedPaymentModelException
     * @throws GuzzleException
     */
    public function payment($card)
    {
        $model = 'regular';
        if (isset($this->account->model) && $this->account->model) {
            $model = $this->account->model;
        }
        if ($model == 'regular') {
            $this->makeRegularPayment();
        } elseif ($model == '3d') {
            $this->make3DPayment();
        } else {
            throw new UnsupportedPaymentModelException();
        }

        return $this;
    }

    /**
    * Refund Order
    *
    * @param $meta
    * @return $this
    */
    public function refund(array $meta): QnbPayPos
    {
       
    }

    /**
    * Cancel Order
    *
    * @param array $meta
    * @return $this
    */
    public function cancel(array $meta): QnbPayPos
    {
        
    }
    /**
    * Make 3D Pay Payment
    *
    * @return $this
    */
    public function make3DPayPayment(): QnbPayPos
    {
    }
	    
    /**
     * generateHashKey
     *
     * @param  mixed $total
     * @param  mixed $installment
     * @param  mixed $currency_code
     * @param  mixed $merchant_key
     * @param  mixed $invoice_id
     * @param  mixed $app_secret
     * @return void
     */
    protected function generateHashKey($total,$installment,$currency_code,$merchant_key,$invoice_id, $app_secret)
    {
        $data 				= $total.'|'.$installment.'|'.$currency_code.'|'.$merchant_key.'|'.$invoice_id;
        $iv 				= substr(sha1(mt_rand()), 0, 16);
        $password 			= sha1($app_secret);
        $salt 				= substr(sha1(mt_rand()), 0, 4);
        $saltWithPassword 	= hash('sha256', $password . $salt);
        $encrypted 			= openssl_encrypt("$data", 'aes-256-cbc', "$saltWithPassword", null, $iv);
        $msg_encrypted_bundle = "$iv:$salt:$encrypted";
        $msg_encrypted_bundle = str_replace('/', '__', $msg_encrypted_bundle);

        return $msg_encrypted_bundle;
    }
    
    /**
     * generateRefundHashKey
     *
     * @param  mixed $invoice_id
     * @param  mixed $merchant_key
     * @param  mixed $app_secret
     * @return void
     */
    protected function generateRefundHashKey($invoice_id, $merchant_key, $app_secret) {
        $data 				= $invoice_id.'|'.$merchant_key;
        $iv 				= substr(sha1(mt_rand()), 0, 16);
        $password 			= sha1($app_secret);
        $salt 				= substr(sha1(mt_rand()), 0, 4);
        $saltWithPassword 	= hash('sha256', $password . $salt);
        $encrypted 			= openssl_encrypt(
            "$data", 'aes-256-cbc', "$saltWithPassword", null, $iv
        );
        $msg_encrypted_bundle = "$iv:$salt:$encrypted";
        $hash_key = str_replace('/', '__', $msg_encrypted_bundle);
        return $hash_key;
    }
    /**
     * Create Regular Payment Post
     *
     * @return object
     */
    protected function createRegularPaymentPOST()
    {
        $IsLive     = $this->mode=='TEST'?false:true;
        $token 		= $this->getToken()->data;
        $environment_url = $IsLive ? 'https://portal.qnbpay.com.tr/ccpayment/api/paySmart2D' : 'https://test.qnbpay.com.tr/ccpayment/api/paySmart2D';
        $parameters = [
            "cc_holder_name" => $this->card->name,
            "cc_no" => $this->card->number,
            "expiry_month" => $this->card->month,
            "expiry_year" => $this->card->year,
            "cvv" => $this->card->cvv,
            "currency_code" => $this->order->currency,
            "installments_number" => $this->order->installment>1 ?$this->order->installment:1,
            "invoice_id" => $this->order->id,
            "invoice_description" => $this->order->id." Nolu ödeme",
            "total" => $this->amountFormat($this->order->amount),
            "merchant_key" => $this->account->merchant_key,
            "items" => $this->order->order_list??[],
            "name" => $this->order->name,
            "surname" => $this->order->name,
            "is_comission_from_user"=>$this->account->vade_fark?"1":"0",
            "commission_by"=>$this->account->vade_fark?"user":"",
            "hash_key" =>$this->generateHashKey($this->amountFormat($this->order->amount),($this->order->installment>1 ?$this->order->installment:1), $this->order->currency, $this->account->merchant_key, $this->order->id, $this->account->api_sec),
        ];
        $options = array (
            CURLOPT_HTTPHEADER => ['Accept: application/json', 'Content-Type: application/json', "Authorization: Bearer $token->token"],
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_VERBOSE => false,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($parameters),
            //CURLOPT_SSL_VERIFYHOST => 0,
            //CURLOPT_SSL_VERIFYPEER => 0,
        );

        $ch = curl_init( $environment_url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err = curl_errno( $ch );
        $errmsg = curl_error( $ch );
        $header = curl_getinfo( $ch );
        $rurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close( $ch );

        $this->data  = json_decode($content);
        return $this;
    }
	    
    /**
     * checkStatus
     *
     * @param  mixed $invoice_id
     * @return void
     */
    protected function checkStatus($invoice_id)
    {
        $IsLive     	= $this->mode=='TEST'?false:true;
        $api_secret 	= $this->account->api_sec;
        $merchant_key 	= $this->account->merchant_key; 

        $hash_key = $this->generateRefundHashKey($invoice_id, $merchant_key, $api_secret);
        $token = $this->getToken()->data->token;
        $headers = ['Accept: application/json', 'Content-Type: application/json', "Authorization: Bearer {$token}"];
        $url = $IsLive ? 'https://portal.qnbpay.com.tr/ccpayment/api/checkstatus' : 'https://test.qnbpay.com.tr/ccpayment/api/checkstatus';

        $array = [
            'invoice_id' => $invoice_id,
            'merchant_key' => $merchant_key,
            'hash_key' => $hash_key,
            'include_pending_status' => "true",
        ];

        return $this->getCurl($url, 'POST', json_encode($array), $headers);
    }
    /**
     * Regular Payment
     *
     * @return $this
     *//**
     * Regular Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function makeRegularPayment()
    {
        $this->createRegularPaymentPOST();
        $response 	= $this->data;
        $status 	= 'declined';
        $message	= '';
        if($response->status_code == 100){
            $status = $this->checkStatus($response->data->invoice_id);

            if($status->status_code == 100 || $status->status_code == 69){
                $status = 'approved';
            }
        } else {
            $message = $response->status_description;
        }

        $this->response = (object) [
            'id'                => isset($response->data->order_id) ? $this->printData($response->data->order_id) : null,
            'reference_no'      => isset($response->data->order_no) ? $this->printData($response->data->order_no) : null,
            'order_id'          => isset($response->data->order_id) ? $this->printData($response->data->order_id) : null,
            'response'          => isset($response->data->status_description) ? $this->printData($this->data->status_description) : isset($response->data->error) ? $this->printData($response->data->error) : null,
            'transaction_type'  => $this->type,
            'code'              => $response->status_code,
            'status'            => $status,
            'error_code'        => isset($response->data->error_code) ? $this->printData($response->data->error_code) : null,
            'error_message'     => isset($response->data->error) ? $this->printData($response->data->error) : null,
            'comission'         => isset($response->data->user_commission) ? $this->printData($response->data->user_commission) : null,
            'all'               => $this->data,
            'original'          => $this->data
        ];

        return $this;
    }
    /**
    * Order Status
    *
    * @param array $meta
    * @return $this
    */
    public function status(array $meta): QnbPayPos
    {
    }

    /**
     * Send contents to WebService
     *
     * @param $contents
     * @return $this
     */
    public function send($contents): QnbPayPos
    {
    }

    /**
    * Order History
    *
    * @param array $meta
    * @return $this
    */
    public function history(array $meta): QnbPayPos
    {
    }

    /**
    * Get Installment List
    *
    * @param array $meta
    * @return $this
    */
    public function getInstallmentList(array $meta): QnbPayPos
    {
        $token 			= $this->getToken()->data;
        $IsLive     	= $this->mode=='TEST'?false:true;
        $api_secret 	= $this->account->api_sec;
        $merchant_key 	= $this->account->merchant_key; 
        $pos_post = [
            'credit_card' => $meta['number'],
            'amount' => $meta['amount'],
            "currency_code" => isset($this->account->currency)?$this->account->currency:"TRY",
            "merchant_key" => $this->account->merchant_key,
            'app_id' => $this->account->api_key,
            'app_secret' => $this->account->api_sec,
            "is_comission_from_user"=>$this->account->vade_fark?"1":"0",
            "commission_by"=>$this->account->vade_fark?"user":""
        ];

        $environment_url = $IsLive ? 'https://portal.qnbpay.com.tr/ccpayment/api/getpos' : 'https://test.qnbpay.com.tr/ccpayment/api/getpos';

        $headers = ['Accept: application/json', 'Content-Type: application/json', "Authorization: Bearer $token->token"];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $environment_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($pos_post));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $get_pos_response = json_decode(curl_exec($ch), true);
        curl_close($ch);
		

        if ($get_pos_response['status_code'] == 100) {
            $this->response = (object)[
                'data' => $get_pos_response['data']
            ];

            return $this;
        }else {
            $this->response = (object)[
                'result' => $get_pos_response['status_description'],
                'data' => null
            ];
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAccount(): array
    {
        return $this->account;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @return mixed
     */
    public function getOrder(): array
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function setCard($card=null)
    {
        return $this->card	= $card;
    }
	
    public function getCard()
    {
        return $this->card;
    }
}
