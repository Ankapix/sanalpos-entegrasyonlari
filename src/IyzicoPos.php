<?php

namespace Ankapix\SanalPos;

use GuzzleHttp\Exception\GuzzleException;
use Ankapix\SanalPos\Exceptions\UnsupportedPaymentModelException;
use Ankapix\SanalPos\Exceptions\UnsupportedTransactionTypeException;
use Ankapix\SanalPos\Exceptions\UnknownError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class IyzicoPos
 * @package Ankapix\SanalPos
 */
class IyzicoPos implements PosInterface
{
    use PosHelpersTrait;

    /**
     * @const string
     */
    public const NAME = 'IyzicoPay';

    /**
     * API URL
     *
     * @var string
     */
    public $url;

    /**
     * Response Codes
     *
     * @var array
     */
    public $codes = [
        '10051'    => "Kart limiti yetersiz, yetersiz bakiye",
        '10005'    => "İşlem onaylanmadı",
        '10012'    => "Geçersiz işlem",
        '10041'    => "Kayıp kart, karta el koyunuz",
        '10043'    => "Çalıntı kart, karta el koyunuz",
        '10054'    => "Vadesi dolmuş kart",
        '10084'    => "CVC2 bilgisi hatalı",
        '10057'    => "Kart sahibi bu işlemi yapamaz",
        '10058'    => "Terminalin bu işlemi yapmaya yetkisi yok",
        '10034'    => "Dolandırıcılık şüphesi",
        '10093'   => "Kartınız e-ticaret işlemlerine kapalıdır. Bankanızı arayınız.",
        '10201'   => "Kart, işleme izin vermedi",
        '10204'   => "Ödeme işlemi esnasında genel bir hata oluştu",
        '10206'   => "CVC uzunluğu geçersiz",
        '10207'   => "Bankanızdan onay alınız",
        '10208'   => "Üye işyeri kategori kodu hatalı",
        '10209'   => "Bloke statülü kart",
        '10210'   => "Hatalı CAVV bilgisi",
        '10211'   => "Hatalı ECI bilgisi",
        '10213'   => "BIN bulunamadı",
        '10214'   => "İletişim veya sistem hatası",
        '10215'   => "Geçersiz kart numarası",
        '10216'  => "Bankası bulunamadı",
        '10217'  => "Banka kartları sadece 3D Secure işleminde kullanılabilir",
        '10219'  => "Bankaya gönderilen istek zaman aşımına uğradı",
        '10222'  => "Terminal taksitli işleme kapalı",
        '10223'  => "Gün sonu yapılmalı",
        '10225'  => "Kısıtlı kart",
        '10226'  => "İzin verilen PIN giriş sayısı aşılmış",
        '10227'  => "Geçersiz PIN",
        '10228'  => "Banka veya terminal işlem yapamıyor",
        '10229'  => "Son kullanma tarihi geçersiz",
        '10232'  => "Geçersiz tutar"
    ];

    /**
     * Currencies
     *
     * @var array
     */
    public $currencies = [];

    /**
     * Transaction Type
     *
     * @var string
     */
    public $type;

    /**
     * API Account
     *
     * @var array
     */
    protected $account = [];

    /**
     * Order Details
     *
     * @var array
     */
    protected $order = [];

    /**
     * Credit Card
     *
     * @var object
     */
    protected $card;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Response Raw Data
     *
     * @var object
     */
    protected $data;

    /**
     * Processed Response Data
     *
     * @var mixed
     */
    public $response;

    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Mode
     *
     * @var string
     */
    protected $mode = 'PROD';

    /**
     * API version
     * @var string
     */
    protected $version = '2.0.54';

    /**
     * IyzicoPos constructor.
     *
     * @param array $config
     * @param array $account
     * @param array $currencies
     */
    public function __construct($config, $account, array $currencies)
    {
        $request = Request::createFromGlobals();
        $this->request = $request->request;

        $this->config = $config;
        $this->account = $account;
        $this->currencies = $currencies;

        $this->url = $this->account->env == 'test' ?
            $this->config['urls']['test'] :
            $this->config['urls']['production'];

        if ($this->account->env == 'test') {
            $this->mode = 'TEST';
        }

        return $this;
    }

    /**
     * Amount Formatter
     *
     * @param double $amount
     * @return float
     */
    public function amountFormat($amount)
    {
        return number_format($amount, 2, '.', '');
    }

    /**
     * Create Regular Payment Post
     *
     * @return object
     */
    protected function createRegularPaymentPOST()
    {
        $options = new \Iyzipay\Options();
        $options->setApiKey($this->account->api_key);
        $options->setSecretKey($this->account->secret_key);
        $options->setBaseUrl($this->url);

        $request = new \Iyzipay\Request\CreatePaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setPrice($this->amountFormat($this->order->amount));
        $request->setPaidPrice($this->amountFormat($this->order->amount+$this->order->commission));

        $request->setCurrency(\Iyzipay\Model\Currency::TL);
        $request->setInstallment((int) $this->order->installment?:1);
        $request->setBasketId($this->order->id);
        $request->setConversationId($this->order->id);

        $paymentCard = new \Iyzipay\Model\PaymentCard();
        $paymentCard->setCardHolderName($this->card->name);
        $paymentCard->setCardNumber($this->card->number);
        $paymentCard->setExpireMonth($this->card->month);
        $paymentCard->setExpireYear($this->card->year);
        $paymentCard->setCvc($this->card->cvv);
        $paymentCard->setRegisterCard(0);
        $request->setPaymentCard($paymentCard);

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($this->order->id);
        $buyer->setName($this->order->name);
        $buyer->setSurname($this->order->surname);
        $buyer->setEmail((string) filter_var($this->order->email, FILTER_VALIDATE_EMAIL)?$this->order->email:"msn@msn.com");
        $buyer->setIdentityNumber("11111111111");
        $buyer->setRegistrationAddress(".");
        $buyer->setIp(isset($this->order->ip) ?$this->order->ip:$this->getIpAdress());
        $buyer->setCity($this->order->city);
        $buyer->setCountry($this->order->country);
        $request->setBuyer($buyer);

        $Address = new \Iyzipay\Model\Address();
        $Address->setContactName($this->order->name);
        $Address->setCity($this->order->city);
        $Address->setCountry($this->order->country);
        $Address->setAddress(".");
        $request->setShippingAddress($Address);
        $request->setBillingAddress($Address);

        $basketItems = [];
        foreach($this->order->order_list as $Bas){
            $basketItem = new \Iyzipay\Model\BasketItem();
            $basketItem->setId($Bas['code']);
            $basketItem->setName($Bas['title']);
            $basketItem->setCategory1($Bas['category']);
            $basketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
            $basketItem->setPrice($this->amountFormat($Bas['amount']));
            $basketItems[] = $basketItem;
        }
        $request->setBasketItems($basketItems);

        $payment = \Iyzipay\Model\Payment::create($request, $options);

        $this->data         = $payment;
        return $this;
    }
    /**
     * Create 3D Payment POST
     * @return object
     */
    protected function create3DPaymentPOST()
    {
        $paymentId 	        = (string) $this->request->get('paymentId');
        $conversationId 	= (string) $this->request->get('conversationId');
        $conversationData 	= (string) $this->request->get('conversationData');

        $options = new \Iyzipay\Options();
        $options->setApiKey($this->account->api_key);
        $options->setSecretKey($this->account->secret_key);
        $options->setBaseUrl($this->url);

        $request = new \Iyzipay\Request\CreateThreedsPaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setPaymentId($paymentId);
        $request->setConversationId($conversationId);
        $request->setConversationData($conversationData);

        $payment = \Iyzipay\Model\ThreedsPayment::create($request, $options);

        $this->data         = $payment;
        return $this;
    }

    /**
     * Get ProcReturnCode
     *
     * @return string|null
     */
    protected function getProcReturnCode()
    {
        return method_exists($this->data, 'getErrorCode') ? (string) $this->data->getErrorCode() : null;
    }

    /**
     * Get Status Detail Text
     *
     * @return string|null
     */
    protected function getStatusDetail(): ?string
    {
        $proc_return_code = $this->getProcReturnCode();

        return $proc_return_code ? (isset($this->codes[$proc_return_code]) ? (string) $this->codes[$proc_return_code] : null) : null;
    }

    /**
     * Regular Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function makeRegularPayment()
    {
        $this->createRegularPaymentPOST();
        $status = 'declined';
        $response = method_exists($this->data, 'getErrorMessage') ? $this->data->getErrorMessage() : 'Başarısız İşlem';
        if ($this->getProcReturnCode() == null and method_exists($this->data, 'getStatus') and $this->data->getStatus() == 'success') {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object) [
            'id'        		=> method_exists($this->data, 'getPaymentId') ? $this->data->getPaymentId() : null,
            'status'            => method_exists($this->data, 'getStatus') ? $this->data->getStatus() : null,
            'error_code'        => $this->getProcReturnCode(),
            'error_message'     => method_exists($this->data, 'getErrorMessage') ? $this->data->getErrorMessage() : null,
            'status_detail'     => $this->getStatusDetail(),
            'status'            => $status,
            'response'          => $response,
            'comission'         => method_exists($this->data, 'getIyziCommissionFee') ? $this->data->getIyziCommissionFee() : null,
            'instalment'        => method_exists($this->data, 'getInstallment') ? $this->data->getInstallment() : null,
            'paid_price'        => method_exists($this->data, 'getPaidPrice') ? $this->data->getPaidPrice() : null,
            'price'             => method_exists($this->data, 'getPrice') ? $this->data->getPrice() : null,
            'all'               => (array) $this->data
        ];

        return $this;
    }

    /**
     * Make 3D Payment
     *
     * @return $this
     * @throws GuzzleException
     */
    public function make3DPayment()
    {
        $this->create3DPaymentPOST();
        $status = 'declined';
        $response = method_exists($this->data, 'getErrorMessage') ? $this->data->getErrorMessage() : 'Başarısız İşlem';
        if ($this->getProcReturnCode() == null and method_exists($this->data, 'getStatus') and $this->data->getStatus() == 'success') {
            $status = 'approved';
            $response = 'Başarılı İşlem';
        }

        $this->response = (object) [
            'id'        		=> method_exists($this->data, 'getPaymentId') ? $this->data->getPaymentId() : null,
            'status'            => method_exists($this->data, 'getStatus') ? $this->data->getStatus() : null,
            'error_code'        => $this->getProcReturnCode(),
            'error_message'     => method_exists($this->data, 'getErrorMessage') ? $this->data->getErrorMessage() : null,
            'status_detail'     => $this->getStatusDetail(),
            'status'            => $status,
            'response'          => $response,
            'comission'         => method_exists($this->data, 'getIyziCommissionFee') ? $this->data->getIyziCommissionFee() : null,
            'instalment'        => method_exists($this->data, 'getInstallment') ? $this->data->getInstallment() : null,
            'paid_price'        => method_exists($this->data, 'getPaidPrice') ? $this->data->getPaidPrice() : null,
            'price'             => method_exists($this->data, 'getPrice') ? $this->data->getPrice() : null,
            'all'               => (array) $this->data
        ];

        return $this;
    }

    /**
    * Get 3d Form Data
    *
    * @return array
    */
    public function get3DFormData()
    {
        $options = new \Iyzipay\Options();
        $options->setApiKey($this->account->api_key);
        $options->setSecretKey($this->account->secret_key);
        $options->setBaseUrl($this->url);

        $request = new \Iyzipay\Request\CreatePaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setCallbackUrl($this->order->url);
        $request->setPrice($this->amountFormat($this->order->amount));
        $request->setPaidPrice($this->amountFormat($this->order->amount+$this->order->commission));

        $request->setCurrency(\Iyzipay\Model\Currency::TL);
        $request->setInstallment((int) $this->order->installment?:1);
        $request->setBasketId($this->order->id);
        $request->setConversationId($this->order->id);

        $paymentCard = new \Iyzipay\Model\PaymentCard();
        $paymentCard->setCardHolderName($this->card->name);
        $paymentCard->setCardNumber($this->card->number);
        $paymentCard->setExpireMonth($this->card->month);
        $paymentCard->setExpireYear($this->card->year);
        $paymentCard->setCvc($this->card->cvv);
        $paymentCard->setRegisterCard(0);
        $request->setPaymentCard($paymentCard);

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($this->order->id);
        $buyer->setName($this->order->name);
        $buyer->setSurname($this->order->surname);
        $buyer->setEmail((string) filter_var($this->order->email, FILTER_VALIDATE_EMAIL)?$this->order->email:"msn@msn.com");
        $buyer->setIdentityNumber("11111111111");
        $buyer->setRegistrationAddress(".");
        $buyer->setIp(isset($this->order->ip) ?$this->order->ip:$this->getIpAdress());
        $buyer->setCity($this->order->city);
        $buyer->setCountry($this->order->country);
        $request->setBuyer($buyer);

        $Address = new \Iyzipay\Model\Address();
        $Address->setContactName($this->order->name);
        $Address->setCity($this->order->city);
        $Address->setCountry($this->order->country);
        $Address->setAddress(".");
        $request->setShippingAddress($Address);
        $request->setBillingAddress($Address);

        $basketItems = [];
        foreach($this->order->order_list as $Bas){
            $basketItem = new \Iyzipay\Model\BasketItem();
            $basketItem->setId($Bas['code']);
            $basketItem->setName($Bas['title']);
            $basketItem->setCategory1($Bas['category']);
            $basketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
            $basketItem->setPrice($this->amountFormat($Bas['amount']));
            $basketItems[] = $basketItem;
        }
        $request->setBasketItems($basketItems);

        $payment = \Iyzipay\Model\ThreedsInitialize::create($request, $options);

        return [
            'status'        => $payment->getStatus(),
            'code'          => $payment->getErrorCode(),
            'message'       => $payment->getErrorMessage(),
            'html_content'  => $payment->getHtmlContent(),
        ];
    }
    /**
    * Get 3d Form 
    *
    * @return mixed
    * @throws UnknownError
    */
    public function get3DForm()
    {
        $form_data = (array) $this->get3DFormData();
        if ($form_data['status']!='success' or !$form_data['html_content']) {
                throw new UnknownError(isset($form_data['message']) ? $this->printData($form_data['message']):"Operation Failed, Please try again.", 400);
        }
        return $form_data['html_content'];
    }

    /**
     * Prepare Order
     *
     * @param object $order
     * @param object null $card
     * @return mixed
     * @throws UnsupportedTransactionTypeException
     */
    public function prepare($order, $card = null)
    {
        $this->order = $order;
        $this->card = $card;

        if ($this->card) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
        }
    }

    /**
     * Make Payment
     *
     * @param object $card
     * @return mixed
     * @throws UnsupportedPaymentModelException
     * @throws GuzzleException
     */
    public function payment($card)
    {
        $this->card = $card;
        if (count((array) $card)) {
            $this->card->month = str_pad($this->card->month, 2, '0', STR_PAD_LEFT);
            $this->card->year  = str_pad($this->card->year, 4, '20', STR_PAD_LEFT);
        }

        $model = 'regular';
        if (isset($this->account->model) && $this->account->model) {
            $model = $this->account->model;
        }

        if ($model == 'regular') {
            $this->makeRegularPayment();
        } elseif ($model == '3d') {
            $this->make3DPayment();
        } else {
            throw new UnsupportedPaymentModelException();
        }

        return $this;
    }

    /**
    * Refund Order
    *
    * @param $meta
    * @return $this
    * @throws GuzzleException
    */
    public function refund(array $meta)
    {
    }

    /**
    * Cancel Order
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function cancel(array $meta)
    {
        
    }

    /**
    * Make 3D Pay Payment
    *
    * @return $this
    */
    public function make3DPayPayment()
    {
    }

    /**
    * Order Status
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function status(array $meta)
    {
    }

    /**
    * Send contents to WebService
    *
    * @param $contents
    * @return $this
    * @throws GuzzleException
    */
    public function send($contents)
    {
    }

    /**
    * Order History
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function history(array $meta)
    {
    }

    /**
    * Installment List
    *
    * @param array $meta
    * @return $this
    * @throws GuzzleException
    */
    public function getInstallmentList(array $meta)
    {
        $options = new \Iyzipay\Options();
        $options->setApiKey($this->account->api_key);
        $options->setSecretKey($this->account->secret_key);
        $options->setBaseUrl($this->url);

        $request = new \Iyzipay\Request\RetrieveInstallmentInfoRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId(time());
        $request->setBinNumber($meta['number']);
        $request->setPrice((double) $this->amountFormat(isset($meta['amount']) ? $meta['amount'] : 0));

        $result = \Iyzipay\Model\InstallmentInfo::retrieve($request, $options);
        $this->response = (object) [
            'data'      => $result->getInstallmentDetails()?$result->getInstallmentDetails()[0]->getInstallmentPrices():null,
            'result'    => $result->getErrorMessage()
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
    * setCard
    *
    * @return $this
    */
    public function setCard($card=null)
    {
    }
}
